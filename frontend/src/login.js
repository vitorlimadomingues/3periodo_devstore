import React from "react"
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import LoginService from './services/login/loginservice';
import { Row, Col } from 'react-bootstrap';
import EquipeService from './services/equipe/equipeservice';


class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      senha: ""
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleChange(event) {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit(event) {
    debugger;
    event.preventDefault();
    debugger;
    LoginService.login(this.state.email, this.state.senha)
      .then(response => {
        if (response.status === 200 & response.data.idTipoUsuario === 1) {
          sessionStorage.setItem("id_tipo_usuario", response.data.idTipoUsuario);
          Swal.fire({
            icon: 'success',
            title: 'Scrum Master Logado'
          }).then(function () {
            sessionStorage.setItem("id_usuario", response.data.id)

            EquipeService.consultarEquipe(response.data.id)
              .then(resp => {
                sessionStorage.setItem("id_equipe", resp.data.idEquipe)
              })

            window.location = "./PaginaInicialScrum";
          });
        } else if (response.status === 200 & response.data.idTipoUsuario === 2) {
          sessionStorage.setItem("id_tipo_usuario", response.data.idTipoUsuario);
          Swal.fire({
            icon: 'success',
            title: 'Dev Logado'
          }).then(function () {
            sessionStorage.setItem("id_usuario", response.data.id)
            window.location = "./PaginaInicialScrum";
          });
        }
        else {
          Swal.fire({
            icon: 'error',
            title: 'E-mail ou Senha incorretos'
          })
        }
      })
      .catch(response => {
        Swal.fire({
          icon: 'error',
          title: 'Erro ao efetuar login',
        })
      })

  }

  render() {

    return (

      <div>

        <Container style={{ paddingLeft: "5%", paddingTop: "1%", paddingBottom: "50px" }}>
          <Row className="justify-content-md-center">
            <Col></Col>
            <Col md="auto" style={{ minWidth: "500px", padding: "50px 20px" }}>
              <Form onSubmit={this.handleSubmit}>
                <Form.Label className="title"> Login </Form.Label>

                <Form.Group className="mb-3">
                  <Form.Label>E-mail</Form.Label>
                  <Form.Control type="text" id="email" placeholder="E-mail" onChange={this.handleChange} value={this.state.email} />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Senha</Form.Label>
                  <Form.Control type="password" id="senha" placeholder="Senha" onChange={this.handleChange} value={this.state.senha} />
                </Form.Group>
                <Form.Group>
                  <Row >
                    <Col style={{ paddingTop: "10px" }}>
                      <Link to="/cadastro" >
                        <Button className="button" variant="outline-primary" type="submit" >
                          Novo Scrum
                        </Button>
                      </Link>
                    </Col>
                    <Col style={{ paddingTop: "10px", align: "Right" }} align="Right">
                      <Button className="button" variant="primary" type="submit">
                        Entrar
                      </Button>
                    </Col>
                  </Row>
                </Form.Group>
              </Form>
            </Col>
            <Col></Col>
          </Row>

        </Container>

      </div>

    );


  }


}

export default Login;
