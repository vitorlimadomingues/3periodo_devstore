import React from "react"
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import { Link, Redirect } from 'react-router-dom';
import CadastroService from './services/login/cadastroservice';
import Swal from 'sweetalert2';
import Cabecalho from './cabecalho';
import { Row, Col } from "react-bootstrap";

function cadastrar(usuario) {

  debugger;
  CadastroService.cadastro(usuario)
    .then(response => {
      debugger;
      if (response.status === 200 && response.data.id != null) {
        Swal.fire({
          icon: 'success',
          title: 'Usuário cadastrado com sucesso'
        }).then(function () {
          debugger;
          sessionStorage.setItem("id_usuario", response.data.id)
          window.location = "/";
        });
      }
      else {
        debugger;
        Swal.fire({
          icon: 'error',
          title: 'Usuário não cadastrado'
        })
      }
    })
    .catch(response => {
      Swal.fire({
        icon: 'error',
        title: 'Erro ao cadastrar usuário',
      })
    })
}

class Cadastro extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      nome: "",
      email: "",
      senha: "",
      confSenha: "",
      idTipoUsuario: '1',
      idStatusUsuario: '1'
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    debugger;
    let usuario = {
      nome: this.state.nome,
      email: this.state.email,
      senha: this.state.senha,
      idTipoUsuario: this.state.idTipoUsuario,
      idStatusUsuario: this.state.idStatusUsuario
    };
    debugger;
    if (this.state.senha !== this.state.confSenha) {
      debugger;
      Swal.fire({
        icon: 'error',
        title: 'As senhas estão diferentes'
      })
    }
    else {
      cadastrar(usuario);
    }

  }

  render() {

    return (
      <div>

        <Container style={{ paddingLeft: "5%", paddingTop: "1%"}}>
        <Row className="justify-content-md-center">
          <Col></Col>
          <Col md="auto" style={{ minWidth: "500px", padding: "50px 20px", paddingBottom:0 }}>
          <Form onSubmit={this.handleSubmit}>
            <Form.Label className="title"> Cadastro </Form.Label>

            <Form.Group className="mb-3">
              <Form.Label>Nome</Form.Label>
              <Form.Control type="text" id="nome" placeholder="Nome" onChange={this.handleChange} value={this.state.nome} />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>E-mail</Form.Label>
              <Form.Control type="text" id="email" placeholder="E-mail" onChange={this.handleChange} value={this.state.email} />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Senha</Form.Label>
              <Form.Control className="md-6" type="password" id="senha" placeholder="Senha" onChange={this.handleChange} value={this.state.senha} />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Confirmar Senha</Form.Label>
              <Form.Control type="password" id="confSenha" placeholder="Confirmar Senha" onChange={this.handleChange} value={this.state.confSenha} />
            </Form.Group>
            <Form.Group>
            <Row >
                <Col style={{ paddingTop: "10px" }}>

            <Link to="/">
              <Button className="button" variant="primary" type="submit">
                Voltar
              </Button>
            </Link>
            </Col>
            <Col style={{ paddingTop: "10px", align: "Right" }} align="Right">
            <Button className="button" variant="primary" type="submit">
              Finalizar
            </Button>
            </Col>
            </Row>
            </Form.Group>


          </Form>
          </Col>
          <Col></Col>
        </Row>
        </Container>
      </div>
    );
  }
}

export default Cadastro;
