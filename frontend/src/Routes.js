import React from "react";
import { Route, BrowserRouter } from "react-router-dom";

import AtividadeScrum from "./atividadeScrum";

const Routes = () => {
    return (
        <BrowserRouter>
           <Route component = { AtividadeScrum }  path="/atividadeScrum" exact />
       </BrowserRouter>
    )
}

export default Routes;