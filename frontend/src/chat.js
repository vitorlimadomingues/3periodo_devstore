import React, { useEffect, useState } from "react";
import io from "socket.io-client";
import { v4 as uuidv4 } from "uuid";
import { format } from 'timeago.js';

import "./chat.css";

const myId = uuidv4();
const Socket = io("http://127.0.0.1:8080", { transports: ["websocket"] });
Socket.on("connect", () =>
  console.log("[IO] Connect => A new connection has been established")
);

const Chat = () => {
  const [message, updateMessage] = useState("");
  const [messages, updateMessages] = useState([]);

  useEffect(() => {
    const handleNewMessage = (newMessage) =>
      updateMessages([...messages, newMessage]);
    Socket.on("chat.message", handleNewMessage);
    updateScroll();
    return () => Socket.off("chat.message", handleNewMessage);
  }, [messages]);

  const handleFormSubmit = (event) => {
    event.preventDefault();
    if (message.trim()) {
      Socket.emit("chat.message", {
        id: myId,
        message,
        data: Date.now(),
      });
      updateMessage("");
    }
  };

  function updateScroll() {
    var element = document.getElementById("mensagens");
    element.scrollTop = element.scrollHeight;
  }

  const handleInputChange = (event) => updateMessage(event.target.value);

  return (
    <main className="containerchat">
      <ul className="list" id="mensagens">
        {messages.map((m, index) => (
          <li
            className={`list__item list__item--${
              m.id === myId ? "mine" : "other"
            }`}
            key={index}
          >
            <span className="username">{m.id === myId ? "Você" : "outro usuário"}</span>
            <span
              id="mensagensid"
              className={`message message--${m.id === myId ? "mine" : "other"}`}
            >
              {m.message}
            </span>
            <span className="message-date">{format(m.data)}</span>
          </li>
        ))}
      </ul>
      <form className="form" onSubmit={handleFormSubmit}>
        <input
          className="form__field"
          placeholder="Digite aqui sua mensagem"
          onChange={handleInputChange}
          type="text"
          value={message}
        />
      </form>
    </main>
  );
};

export default Chat;
