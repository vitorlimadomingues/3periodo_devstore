import React from "react";
import { Row, Col } from "react-bootstrap";

import BoxTarefa from "./BoxTarefa";
import AddTarefas from "./AddTarefas";

import "./BoxTarefas.css";

const BoxTarefas = ({ handleAddTarefa, tarefas, handleExcluirTarefa }) => {

  return (
    <>
      <Row className="tarefas" xs={1} md={5}>
        {["Aumentar", "Manter", "Diminuir", "Parar", "Começar"].map((tipo) => (
          <Col className={`tarefa tarefa_${tipo}`}>
            <h6>{tipo}</h6>
            <BoxTarefa tipo={tipo} handleAddTarefa={handleAddTarefa} />
            <AddTarefas tarefas={tarefas} tipo={tipo} handleExcluirTarefa={handleExcluirTarefa} />
          </Col>
        ))}
      </Row>
    </>
  );
};

export default BoxTarefas;
