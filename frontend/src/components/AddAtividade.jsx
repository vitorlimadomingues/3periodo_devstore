import React, { useState } from "react";

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { Col } from "react-bootstrap";
import { Link } from "react-router-dom";

import "./AddAtividade.css";
import AtividadeService from "../services/atividade/AtividadeService";

const AddAtividade = ({ handleAddAtividade, atividades}) => {
  const [inputTitulo, setInputTitulo] = useState("");
  const [inputDescricao, setInputDescricao] = useState("");
  const [atividadeId, setAtividadeId] = useState(0);


  const handleInputChangeTitulo = (e) => {
    setInputTitulo(e.target.value);
  };

  const handleInputChangeDescricao = (e) => {
    setInputDescricao(e.target.value);
  };

  const handleAddAtividadeClick = (event) => {
    
    let ativ = {
       idAtividade: null,
       idSala: localStorage.getItem("idSala"),
       tituloAtividade: inputTitulo, 
       descricaoAtividade: inputDescricao,
       pontuacaoAtividade: 0,
       votacaoAtiva: false
    }
      
    AtividadeService.cadastroAtividade(ativ)  
      .then(response => {
        if(response.status === 200){;
            setAtividadeId(response.data); 
        }
    });

    handleAddAtividade(inputTitulo, inputDescricao, atividadeId);
    
    setInputTitulo("");
    setInputDescricao("");
    //setAtividadeId(0);

  };

  return (
    <div className="add-atividade-container">
      <div className="linha-1-titulo">
        <Form.Label>Título:</Form.Label>
        <input
          className="add-titulo-input"
          type="text"
          value={inputTitulo}
          onChange={handleInputChangeTitulo}
        ></input>
      </div>
      <div className="linha-2-descricao">
        <Form.Label>Descrição:</Form.Label>

        <input
          className="add-desc-input"
          type="text"
          value={inputDescricao}
          onChange={handleInputChangeDescricao}
        ></input>
      </div>
      <div style={{ display: "flex", paddingTop: "10px" }}>
        <Col>
          <Link to="/PaginaInicialScrum">
            <Button className="button" variant="outline-primary">
              Voltar
            </Button>
          </Link>
        </Col>
        <Col style={{ align: "Right" }} align="Right">
          <Button
            type="submit"
            className="button"
            variant="primary"
            onClick={handleAddAtividadeClick}
          >
            Adicionar
          </Button>
        </Col>
      </div>
    </div>
  );
};

export default AddAtividade;
