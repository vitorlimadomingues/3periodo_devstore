import React from "react";
import Card from "react-bootstrap/Card";
import { CloseButton } from "react-bootstrap";

import "./AddTarefas.css";

const AddTarefas = ({ tarefas, tipo, handleExcluirTarefa }) => {
  return (
    <ul className="cards_tarefas">
      {tarefas.map((tarefa) =>
        tarefa.tipo === tipo ? (
          <li>
            <Card className="cardzinho">
              <Card.Body id="card_tarefa">{tarefa.tarefa}</Card.Body>
              <CloseButton
                onClick={() => handleExcluirTarefa(tarefa.id)}
                aria-label="Hide"
                className="button-delete"
                variant="outline-danger"
                size="sm"
              ></CloseButton>
            </Card>
          </li>
        ) : null
      )}
    </ul>
  );
};

export default AddTarefas;
