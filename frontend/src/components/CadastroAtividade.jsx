import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import {  Offcanvas } from "react-bootstrap";

import Atividades from "./Atividades";
import AddAtividade from "./AddAtividade";

import "./CadastroAtividade.css";

const CadastroAtividade = ({
  atividades,
  handleVoteClick,
  handleExcluirAtividade,
  handleAddAtividade,
  handleVoteClickAction,
  nenhumaAtividade,
  ...props
}) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const toggleShow = () => setShow((s) => !s);

  return (
    <>
      <Button variant="dark" onClick={toggleShow} className="me-2">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="32"
          height="32"
          fill="currentColor"
          class="bi bi-justify"
          viewBox="0 0 16 16"
        >
          <path
            fill-rule="evenodd"
            d="M2 12.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"
          />
        </svg>
      </Button>
      <Offcanvas show={show} onHide={handleClose} placement={"end"} {...props}>
        <Offcanvas.Header style={{ alignItems: "center" }}>
          <Offcanvas.Title>
            <i>Cadastro de Atividade</i>
          </Offcanvas.Title>
          <div style={{display: 'flex'}}>
            <button className="botao-deletar">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="24px"
                viewBox="0 0 24 24"
                width="24px"
                fill="#777"
              >
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
              </svg>
            </button>
            <button className="botao-fechar" onClick={handleClose}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="24px"
                viewBox="0 0 24 24"
                width="24px"
                fill="#777"
              >
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
              </svg>
            </button>
          </div>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <AddAtividade 
            handleAddAtividade={handleAddAtividade} 
            atividades={atividades}
            />
          <Atividades
            atividades={atividades}
            handleVoteClick={handleVoteClick}
            handleExcluirAtividade={handleExcluirAtividade}
            handleVoteClickAction={handleVoteClickAction}
            handleClose={handleClose}
            nenhumaAtividade={nenhumaAtividade}
          />
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
};

export default CadastroAtividade;
