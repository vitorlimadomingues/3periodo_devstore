import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';

const BoxTarefa = ({tipo, handleAddTarefa}) => {
    const [show, setShow] = useState(false);

    const [inputTarefa, setInputTarefa] = useState("");
    //const [tipoSelec, setTipoSelec] = useState("");
      
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleAddTarefaClick = () => {
        //setTipoSelec(tipo);
        //console.log(tipo); mostra o tipo que vem de fora
        handleAddTarefa(inputTarefa, tipo);
        setInputTarefa("");
        //setTipoSelec("");
        setShow(false);
      };
    
      const handleInputChangeTarefa = (e) => {
        setInputTarefa(e.target.value);
      };
      
    return (
        <>
            <Button variant="primary" onClick={handleShow}
            style={{ display: "block", 
                     width: "100%", 
                     fontSize: "x-large", 
                     padding: 0,
                     marginBottom: "10px"
            }}>
              +
            </Button>
      
            <Modal show={show} onHide={handleClose} centered>
              <Modal.Header closeButton>
                <Modal.Title>{tipo}</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                  <Form.Group>
                      <Form.Label>Insira uma tarefa</Form.Label>
                      <Form.Control 
                        type="text"
                        value={inputTarefa}
                        onChange={handleInputChangeTarefa} 
                      />
                  </Form.Group>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                  Fechar
                </Button>
                <Button variant="primary" onClick={handleAddTarefaClick}>
                  Salvar
                </Button>
              </Modal.Footer>
            </Modal>
        </>
    )
}

export default BoxTarefa;