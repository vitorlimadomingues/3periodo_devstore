import React from "react";
import { CloseButton } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

import "./Atividade.css";

const Atividade = ({ atividade, handleVoteClick, handleExcluirAtividade, handleVoteClickAction, handleClose }) => {
  return (
    <Card className="card"
      style={
        atividade.votada
          ? { width: "auto", backgroundColor: "#E0FFFF" }
          : { width: "auto" }
      }
    >
      <Card.Body>
        <Card.Title>{atividade.titulo}</Card.Title>
        <Card.Text>{atividade.descricao}</Card.Text>
        <div className="container-buttons">
            <Button 
              className="button-vote"
              onClick={() => {handleVoteClick(atividade.id); handleVoteClickAction(atividade.id); handleClose();}} 
              variant="primary">
                Votar
            </Button>
            <CloseButton
              onClick={() => handleExcluirAtividade(atividade.id)}
              aria-label="Hide"
              className="button-delete"
              variant="outline-danger"
              size="sm"
            ></CloseButton>
        </div>
      </Card.Body>
    </Card>
  );
};

export default Atividade;
