import React from "react";
import Form from "react-bootstrap/Form";

import Atividade from "./Atividade";
import "./Atividades.css";

const Atividades = ({ atividades, handleVoteClick, handleExcluirAtividade, handleVoteClickAction, handleClose, nenhumaAtividade }) => {
    return (
        <>
            <Form.Label hidden={nenhumaAtividade} className="h6">Atividade(s):</Form.Label>
            {atividades.map((atividade) => (
                <Atividade 
                    atividade={atividade} 
                    handleVoteClick={handleVoteClick}
                    handleExcluirAtividade={handleExcluirAtividade} 
                    handleVoteClickAction={handleVoteClickAction}
                    handleClose={handleClose}
                />
            ))}
        </> 
    );
};

export default Atividades;