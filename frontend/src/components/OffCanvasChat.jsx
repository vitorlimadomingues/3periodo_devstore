import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import { Offcanvas } from "react-bootstrap";

import Chat from "../chat";
import "./OffCanvasChat.css";

const OffCanvasChat = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const toggleShow = () => setShow((s) => !s);

  return (
    <>
      <Button variant="dark" onClick={toggleShow} className="me-2">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          height="32px"
          viewBox="0 0 24 24"
          width="32px"
          fill="#fff"
        >
          <path d="M0 0h24v24H0z" fill="none" />
          <path d="M20 2H4c-1.1 0-1.99.9-1.99 2L2 22l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zM6 9h12v2H6V9zm8 5H6v-2h8v2zm4-6H6V6h12v2z" />
        </svg>
      </Button>
      <Offcanvas show={show} onHide={handleClose} placement={"end"} backdrop={false} id="off_chat">
        <Offcanvas.Header style={{ alignItems: "center" }}>
          <Offcanvas.Title>
            <i>Chat</i>
          </Offcanvas.Title>
          <div style={{ display: "flex" }}>
            <button className="botao-fechar" onClick={handleClose}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                height="24px"
                viewBox="0 0 24 24"
                width="24px"
                fill="#777"
              >
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
              </svg>
            </button>
          </div>
        </Offcanvas.Header>
        <Offcanvas.Body id="chat_body">
          <Chat />
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
};

export default OffCanvasChat;
