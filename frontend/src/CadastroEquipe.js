import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import { Link } from 'react-router-dom';
import { Row, Col, Table } from "react-bootstrap";
import Swal from 'sweetalert2';
import EquipeService from './services/equipe/equipeservice';
import { Offcanvas } from "react-bootstrap";
import { CloseButton } from "react-bootstrap";
import UsuariosEquipe from "./UsuariosEquipe";

function cadastrar(usuario_equipe, equipe) {
    //Cadastra a equipe
    
    // if verifica se o SM já tem uma equipe cadastrada ou não. Se não, cria uma nova antes de cadastrar os usuários.
    if ((sessionStorage.getItem('id_equipe') === null) ||
        (sessionStorage.getItem('id_equipe') === 'undefined')) {
        EquipeService.cadastroEquipe(equipe)
        .then(response => {
            if (response.status === 200) {
                console.log('Equipe cadastrada')

                let id_equipe = response.data.idEquipe
                usuario_equipe.map((i) => i.idEquipe = id_equipe);

                sessionStorage.setItem("id_equipe", id_equipe)

                cadastrarEquipe(usuario_equipe)
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Equipe não cadastrada',
                })
            }
        })
        .catch(response => {
            Swal.fire({
                icon: 'error',
                title: 'Erro ao cadastrar equipe',
            })
        })
    } else {
        usuario_equipe.map((i) => i.idEquipe = sessionStorage.getItem('id_equipe'));
        cadastrarEquipe(usuario_equipe)
    }
}

//Função para cadastrar usuários a equipe
function cadastrarEquipe(usuario_equipe) {
    //Cadastra os devs listados ("itens") no banco

    console.log(usuario_equipe);
    EquipeService.cadastroUsuarioEquipe(usuario_equipe)
        .then(response => {
            if (response.status === 200) {
                Swal.fire({
                    icon: 'success',
                    title: 'Equipe cadastrada com sucesso'
                }).then(function () {
                    window.location = "/PaginaInicialScrum";
                });
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Usuários não cadastrados'
                })
            }
        })
        .catch(response => {
            Swal.fire({
                icon: 'error',
                title: 'Erro ao cadastrar usuários',
            })
        })
}

export default function CadastroEquipe() {

    //Verificar se o usuário está logado ao acessar a página ou se o usuário é um Scrum Master
    if (sessionStorage.getItem('id_usuario') == null) {
        Swal.fire({
            icon: 'info',
            title: 'Você não está logado!'
        }).then(function () {
            window.location = "/";
        });
    } else if (sessionStorage.getItem('id_tipo_usuario') == 2) {
        Swal.fire({
            icon: 'info',
            title: 'Você não é um Scrum Master!'
        }).then(function () {
            window.location = "/";
        });
    }

    const [items, setItems] = useState([]);
    const [itemName, setItemName] = useState("");
    const [equipe, setEquipe] = useState(true);

    const [show, setShow] = useState(false);
    const [listaUsuariosEquipe, setListaUsuariosEquipe] = useState([]);

    const handleClose = () => setShow(false);
    const toggleShow = () => setShow((s) => !s);

    //Adicionar item a listagem (antes de cadastrar)
    const addItem = event => {
        event.preventDefault();

        let itemDuplicado = false;

        items.map((item) => {
            if (item.name === itemName) {
                itemDuplicado = true;
            }
        });

        if (!itemDuplicado) {

            setEquipe(false);

            setItems([
                ...items,
                {
                    id: items.length + 1,
                    name: itemName,
                    idEquipe: null
                }
            ]);
            setItemName("");
        } else {
            alert("Você já inseriu esse e-mail na equipe!!!");
        }

    };

    //Remover item da listagem (antes de cadastrar)
    const handleRemoveItem = (id) => {

        const novaLista = items.filter(item => item.id !== id);
        setItems(novaLista);

        if (novaLista.length === 0)
            setEquipe(true);
    };

    //Cadastrar a equipe
    const handleSubmit = () => {
        debugger;

        let usuario_equipe = items

        let equipe = {
            idEquipe: null,
            idScrum: sessionStorage.getItem("id_usuario"),
            nomeEquipe: "Equipe " + sessionStorage.getItem("id_usuario")
        };

        cadastrar(usuario_equipe, equipe);
    }

    //Listagem dos usuários já cadastrados a equipe do Scrum Master
    const handleListarUsuarios = () => {
        debugger;

        EquipeService.consultarUsuariosEquipe(sessionStorage.getItem('id_equipe'))
            .then(response => {
                setListaUsuariosEquipe(response.data)
            })
    }

    //Remover um usuário. Um usuário removido é do tipo "dev" e o status dele no banco passa pra "removido"
    const handleRemoverUsuario = (id) => {
        const novaLista = listaUsuariosEquipe.filter(u => u.id !== id);
        setListaUsuariosEquipe(novaLista);

        EquipeService.removerUsuariosEquipe(sessionStorage.getItem('id_equipe'), id)
            .then(response => {
                Swal.fire({
                    icon: 'success',
                    title: 'Usuário removido com sucesso!'
                })
            })
    }

    //Remover TODOS os usuários da equipe. Mesmo conceito da função acima.
    const handleRemoverAll = () => {
        var r = window.confirm("Tem certeza que deseja excluir todos da equipe?");
        if (r === true) {
            const novaLista = listaUsuariosEquipe.slice(-1, -1);
            setListaUsuariosEquipe(novaLista);

            EquipeService.removerTodosUsuarios(sessionStorage.getItem('id_equipe'))
            .then(response => {
                Swal.fire({
                    icon: 'success',
                    title: 'Usuários removidos com sucesso!'
                })
            })
        }
    }

    return (
        <><div>
            <Container>
                <Row>
                    <Col style={{ paddingLeft: "35%", paddingTop: "7%", paddingBottom: "50px" }}>
                        <Form onSubmit={addItem}>
                            <Form.Label className="title"> Cadastro de Equipe </Form.Label>
                            <Form.Group className="mb-3 col-md-6" style={{ width: "400px" }}>
                                <Form.Label>E-mail</Form.Label>
                                <Form.Control
                                    type="text"
                                    id="email"
                                    placeholder="E-mail"
                                    value={itemName}
                                    onChange={e => setItemName(e.target.value)} />
                                <Row>
                                    <Col style={{ paddingTop: "10px" }}>
                                        <Link to="/PaginaInicialScrum">
                                            <Button className="button" variant="outline-primary">
                                                Voltar
                                            </Button>
                                        </Link>
                                    </Col>
                                    <Col style={{ paddingTop: "10px", align: "Right" }} align="Right">
                                        <Button className="button" variant="primary" onClick={addItem}>
                                            Adicionar
                                        </Button>
                                    </Col>


                                </Row>
                            </Form.Group>
                        </Form>
                    </Col>
                    <Col style={{ textAlign: "right", padding: "20px 20px" }}>
                        <Button variant="dark" onClick={() => { handleListarUsuarios(); toggleShow() }} className="me-2">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="32"
                                height="32"
                                fill="currentColor"
                                class="bi bi-justify"
                                viewBox="0 0 16 16"
                            >
                                <path
                                    fill-rule="evenodd"
                                    d="M2 12.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"
                                />
                            </svg>
                            Equipe
                        </Button>

                        <Offcanvas show={show} onHide={handleClose} placement={"end"}>
                            <Offcanvas.Header style={{ alignItems: "center" }}>
                                <Offcanvas.Title>
                                    <i>Equipe Cadastrada</i>
                                </Offcanvas.Title>
                                <div style={{ display: 'flex' }}>
                                    <button className="botao-deletar" onClick={handleRemoverAll}>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            height="24px"
                                            viewBox="0 0 24 24"
                                            width="24px"
                                            fill="#777"
                                        >
                                            <path d="M0 0h24v24H0z" fill="none" />
                                            <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
                                        </svg>
                                    </button>
                                    <button className="botao-fechar" onClick={handleClose}>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            height="24px"
                                            viewBox="0 0 24 24"
                                            width="24px"
                                            fill="#777"
                                        >
                                            <path d="M0 0h24v24H0z" fill="none" />
                                            <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
                                        </svg>
                                    </button>
                                </div>
                            </Offcanvas.Header>
                            <Offcanvas.Body>
                                <UsuariosEquipe
                                    usuarios={listaUsuariosEquipe}
                                    handleRemoverUsuario={handleRemoverUsuario}
                                />
                            </Offcanvas.Body>
                        </Offcanvas>
                    </Col>
                </Row>
                <Row>
                    <Col style={{ paddingLeft: "20%", paddingRight: "25%" }} hidden={equipe}>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Nº</th>
                                    <th>E-mail</th>
                                    <th width="50px">Exclusão</th>
                                </tr>
                            </thead>

                            <tbody>
                                {items.map((item, i) => {
                                    return [
                                        <tr>
                                            <td>
                                                {item.id}
                                            </td>
                                            <td>
                                                {item.name}
                                            </td>
                                            <td align="center">
                                                <CloseButton aria-label="Hide" className="button" variant="outline-danger" size="sm" onClick={() => handleRemoveItem(item.id)} />
                                            </td>
                                        </tr>
                                    ];
                                })}
                            </tbody>
                        </Table>
                        <div class="row">
                            <div class="span4" align="center">
                                <Button className="button" variant="primary" onClick={handleSubmit}>Cadastrar Equipe</Button>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
        </>
    );
}