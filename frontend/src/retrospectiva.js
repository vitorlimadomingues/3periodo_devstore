import React, { useState, useEffect, useCallback } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Swal from 'sweetalert2';

import BoxTarefas from "./components/BoxTarefas";
import OffCanvasChat from "./components/OffCanvasChat";
import RetrospectivaService from "./services/retrospectiva/retrospectivaservice";


import "./retrospectiva.css";

const Retrospectiva = () => {
  const [tarefas, setTarefas] = useState([]);
  const [idSala, setIdSala] = useState(null);

  useEffect(() => {
    debugger
    var url = window.location.pathname;
    var getIdSala = url.substring(url.lastIndexOf('/') + 1);
    setIdSala(getIdSala)
    setTarefas([])
    RetrospectivaService.buscarAtividadesSala(getIdSala)
      .then((response) => {
        response.data.map(atividade => {
          const novaTarefa = {
            tarefa: atividade.tituloAtividade,
            tipo: atividade.colunaAtividade,
            id: atividade.id,
          }
          tarefas.push(novaTarefa)
        })
      })
      .catch(() => {
        Swal.fire('Erro ao buscar atividades desta sala', '', 'error')
      })
      .finally(() => { return setTarefas(tarefas) })
  }, [])

  const handleAddTarefa = (tarefa, tipo) => {
    const objAtividade = {
      id: null,
      idSala: idSala,
      tituloAtividade: tarefa,
      colunaAtividade: tipo
    }
    RetrospectivaService.criarAtividade(objAtividade)
      .then((response) => {
        const novaTarefa = [
          ...tarefas,
          {
            tarefa: tarefa,
            tipo: tipo,
            id: response.data.id,
          },
        ];
        setTarefas(novaTarefa);
      })
      .catch(() => {
        Swal.fire('Erro ao cadastrar atividade', '', 'error')
      })
  };

  const handleExcluirTarefa = (tarefaId) => {
    debugger
    RetrospectivaService.excluirAtividade(tarefaId)
      .then(() => {
        const novaTarefa = tarefas.filter(tarefa => tarefa.id !== tarefaId);
        setTarefas(novaTarefa);
      })
      .catch(() => {
        Swal.fire('Erro ao excluir atividade', '', 'error')
      })
  };

  return (
    <>
      <Container flat>
        <Row className="justify-content-md-center">
          <Col md="auto" style={{ width: "1034px", padding: "10px" }}>
            <h1 className="titulo_pagina">Retrospectiva</h1>
            <BoxTarefas
              handleAddTarefa={handleAddTarefa}
              tarefas={tarefas}
              handleExcluirTarefa={handleExcluirTarefa}
            />
          </Col>
          <Col style={{ textAlign: "right", padding: "20px 20px" }}>
            <OffCanvasChat />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Retrospectiva;
