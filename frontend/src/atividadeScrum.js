import React, { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import CardsVotacao from "./CardsVotacao";
import './index.css';

export default function AtividadeScrum() {

    const [tituloEnabled, setTituloEnabled] = useState(true);
    const [descricaoEnabled, setDescricaoEnabled] = useState(true);

    const handleDesabilitarComponetesAtividade=(propos)=> {
        setTituloEnabled(!tituloEnabled);
        setDescricaoEnabled(!descricaoEnabled);
    }

    return (
        
        <Container class="container-principal">

            <Container style={{ paddingLeft: "0%", paddingBotton: "12%", paddingRight: "60%" }}>
                <Form.Group className="mb-3">
                    <Form.Label>Título da atividade</Form.Label>
                    <Form.Control type="text" id="titulo"
                        disabled={(!tituloEnabled) ? "disabled" : ""} />
                    <Form.Label>Descrição da atividade</Form.Label>
                    <Form.Control as="textarea" rows={3} id="descricao" placeholder="Essa atividade visa..."
                        disabled={(!descricaoEnabled) ? "disabled" : ""} />
                    <br />

                    <div class="espacamento">

                        <Link to="/PaginaInicialScrum">
                            <Button id="botao" variant="primary" style={{}}>Cancelar</Button>
                        </Link>
                        <Button variant="primary" onClick={handleDesabilitarComponetesAtividade}>Criar</Button>
                    </div>

                </Form.Group>
            </Container>

            <CardsVotacao />

        </Container>
        
    );
}
