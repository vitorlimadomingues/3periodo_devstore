import React from "react";
import Navbar from "react-bootstrap/Navbar";
import { Container } from "react-bootstrap";

export default function cabecalho() {
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="/">
          <img
            alt=""
            src="/cube.png"
            width="30"
            height="30"
            className="d-inline-block align-top"
          />{" "}
          DevStore
        </Navbar.Brand>
      </Container>
    </Navbar>
    /*<Navbar style={{width: "100%"}} variant="dark" bg="dark" >
            <Container expand="xxl">
                <Navbar.Brand>DevStore</Navbar.Brand>
            </Container>
        </Navbar>*/
  );
}
