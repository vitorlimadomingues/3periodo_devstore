import React, {useState, useEffect} from "react";
import Container from "react-bootstrap/esm/Container";
import Button from "react-bootstrap/Button";
import { Form } from "react-bootstrap";
import SalaService from "./services/sala/SalaService";
import Swal from "sweetalert2";

export default function Sala(){

    const [listaEquipes, setListaEquipes] = useState([]);
    
    useEffect(() => {
        listarEquipes();
        localStorage.setItem("idSala", 0);
    }, [])

    function listarEquipes(){
        SalaService.buscarEquipe()
            .then((response) => {
                setListaEquipes(response.data)
            })
            .catch(() => {
                Swal.fire("Não foi possível listar equipes", "", "error")
            })
    }

    function criarSala(){
        let e = document.getElementById("selectEquipe");
        let selectedEquipeId = e.value;

        if(selectedEquipeId == "Selecione uma Equipe"){
            Swal.fire("Selecione uma Equipe", "", "warning")
        }
        else{
            let sala = {
                id: null,
                equipeDTO: {
                    idEquipe: selectedEquipeId
                },
                cerimoniaDTO: {
                    id: 1
                }
            }
    
            SalaService.criarSala(sala)
                .then((response) => {
                    localStorage.setItem("idSala", response.data.id);
                    enviarEmails(response.data, selectedEquipeId)
                })
                .catch(() => {
                    Swal.fire("Não foi possível criar sala", "", "error")
                })    
        }
    }

    function enviarEmails(sala, equipe){
        SalaService.buscarUsuariosEquipe(equipe)
            .then((response) => {
                let url = "http://localhost:3000/planning-poker/" + sala.id

                let email = {
                    email: null,
                    assunto: "Convite para Planning",
                    corpo: "Olá, você foi convidado para participar do Planning. \n Link de acesso: " + url
                }

                response.data.map((e) => {
                    email.email = e.email;
                    sendEmail(email)
                })
            })
            .catch(() => {
                Swal.fire("Não foi possível buscar a equipe selecionada", "", "error")
            })
            .finally(() => {
                Swal.fire("E-mail de convite enviado para os participantes da equipe", "", "success")
                    .then(() => {
                        window.location.href = "/planning-poker/" + sala.id
                    })
            })
    }

    function sendEmail(email){
        SalaService.enviarEmail(email)
            .catch(() => {
                Swal.fire("Erro ao enviar E-mails", "", "error")
            })
    }

    return(
        <Container style={{padding:'10%'}}>
            <Form.Select aria-label="Default select example" id="selectEquipe">
                <option>Selecione uma Equipe</option>
                {listaEquipes.map((e) => {
                    return <option key={e.idEquipe} value={e.idEquipe}>{e.nomeEquipe}</option> 
                })}
            </Form.Select>
            <center>
                <Button style={{margin:'2%'}} onClick={() => criarSala()}>Criar Sala</Button>
            </center>
        </Container>
    )
}