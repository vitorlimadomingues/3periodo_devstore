import React, { useState } from "react";
import CadastroAtividade from "./components/CadastroAtividade";
import CardsVotacao from "./CardsVotacao";
import OffCanvasChat from "./components/OffCanvasChat";

import VotacaoService from './services/votacao/VotacaoService';
import AtividadeService from "./services/atividade/AtividadeService"
import { Row, Col, Container, Form, Card, Button, InputGroup } from "react-bootstrap";


const PlanningPoker = () => {
  const [atividades, setAtividades] = useState([]);
  const [nenhumaAtividade, setNenhumaAtividade] = useState(true);
  const [cardsStatus, setCardsStatus] = useState(true); // true = hidden
  const [votacao, setVotacao] = useState([]);
  const [controleVotacao, setControleVotacao] = useState(0);
  const [atividadeId, setAtividadeId] = useState(0);
  const [iniciaVotos, setIniciaVotos] = useState(false);
  const [finalizaVotos, setFinalizaVotos] = useState(true);
  const [eScrum, setEScrum] = useState(true);
  //const [eDev, setDev] = useState(true);
  const [tituloVotavao, setTituloVotacao] = useState("");
  const [descricaoVotavao, setDescricaoVotacao] = useState("");
  const [cardsVotos, setCardsVotos] = useState(true);


  const eDev = () =>{
     if(sessionStorage.getItem("id_tipo_usuario") === '2'){
       return true;
     }else{
       return false;
     }
  }

  const buscarAtividade = () => {
     AtividadeService.buscaUltimaAtividade()
       .then(response =>{

        if(response.status === 200){
          if (response.data.votacaoAtiva){
            //document.getElementById("tituloFora").defaultValue = response.data.tituloAtividade;
            //document.getElementById("descricaoFora").defaultValue = response.data.descricaoAtividade;
            setTituloVotacao(response.data.tituloAtividade);
            setDescricaoVotacao(response.data.descricaoAtividade);
            setCardsStatus(false);
            setAtividadeId(response.data.idAtividade);
          }else{

            //document.getElementById("tituloFora").value = " ";
            //document.getElementById("descricaoFora").value = " ";
            setTituloVotacao("");
            setDescricaoVotacao("");
            setCardsStatus(true);
          }
        }
       });  
    
  }

  if(sessionStorage.getItem("id_tipo_usuario") === '2'){
    //setEScrum(true);
    setInterval(() => buscarAtividade(), 5000);
    
  }else{
    //setEScrum(false); 
  }
 
  const buscaVotacao = () => {
    console.log("está entrando aqui");
    VotacaoService.buscarVotacao().then(response => {
      if(response.status === 200){
        let votacoes = [];
        response.data.map((item) =>{
        if(item.usuarioNome !== "")
          votacoes.push(item);    
        });
        /*
        if(votacoes.length === 0)
           return;
         */
        setVotacao(votacoes);
      }
    })
  }

  const handleBuscaVotacao = () => { 
    if(controleVotacao){
      clearInterval(controleVotacao);
      setControleVotacao(0);
      return;
    }
    retornaIdUltimativiade();
    const IdControleVotacao = setInterval(() => buscaVotacao(), 10000);
    setControleVotacao(IdControleVotacao);
  }

  const iniciaVotacao = () =>{
      AtividadeService.updateVotacaoAtiva();
      setIniciaVotos(true);
      setFinalizaVotos(false);
      setCardsVotos(false);
  }

  const finalizaVotacao = () => {
    setIniciaVotos(true);
    setFinalizaVotos(true);
    AtividadeService.updateVotacaoAtiva();
    setCardsVotos(true);
  }

  const handleVoteClick = (atividadeId) => {
    const novaAtividade = atividades.map(atividade => {
      if (atividade.id === atividadeId) return { ...atividade, votada: !atividade.votada };
      return atividade;
    });
    setAtividades(novaAtividade);
    setCardsStatus(false);
  }

  const handleVoteClickAction = (atividadeId) => {
    // eslint-disable-next-line array-callback-return
    atividades.map((atividade) => {
      if (atividade.id === atividadeId) {
        //document.getElementById("tituloFora").defaultValue = atividade.titulo;
        //document.getElementById("descricaoFora").defaultValue = atividade.descricao;
        
        setTituloVotacao(atividade.titulo);
        setDescricaoVotacao(atividade.descricao);
        setIniciaVotos(false);
        setFinalizaVotos(true);
        setEScrum(false);
        if (sessionStorage.getItem("id_tipo_usuario") === '1'){
           setCardsStatus(true);
        }
      }
    });
  }

  const handleExcluirAtividade = (atividadeId) => {
    const novaAtividade = atividades.filter(atividade => atividade.id !== atividadeId);

    AtividadeService.deleteUltimaAtividade()
       .then(response =>{
           if(response === 200){
              console.log("Última atividade excluída");
           }
    });

    setAtividades(novaAtividade);
    if (novaAtividade.length === 0) setNenhumaAtividade(true);
  }

  const handleAddAtividade = (atividadeTitulo, atividadeDescricao) => {
  
    let itemDuplicado = false;
    // eslint-disable-next-line array-callback-return

    let novaAtividade = []
    atividades.map((atividade) => {
      if (atividade.titulo === atividadeTitulo) {
        itemDuplicado = true;
      }
    });

    if (itemDuplicado) {
      alert("O título '" + atividadeTitulo + "' já está sendo utilizado!!!" +
        "\nPor favor, escolha outro título para esta atividade.");
    } else if (atividadeTitulo === "") alert("O Título não pode ser vazio!");
    else {
      
      novaAtividade = [
        ...atividades,
        {
          titulo: atividadeTitulo,
          descricao: atividadeDescricao,
          id: Math.random(10),
          votada: false,
          idAtividade: 0
        }
      ];
      setAtividades(novaAtividade);
      if (novaAtividade !== "") setNenhumaAtividade(false);
    }

  }
  
  const retornaIdUltimativiade = () => {
    AtividadeService.buscaUltimaAtividade()
      .then(response =>{
         if(response.status === 200){
          setAtividadeId(response.data.idAtividade);
         }
      });
  }

  return (
    <>
      <Container flat>
        <Row className="justify-content-md-center">
          <Col md="auto" style={{ minWidth: "700px", padding: "20px" }}>
            <Form>
              <Form.Group as={Row} className="mb-1">
                <Form.Label className="title" >Atividade:</Form.Label>
              </Form.Group>
              <Form.Group as={Row} className="mb-2" controlId="formHorizontalTitulo">
                <Form.Label htmlFor="inlineFormInputGroupTitulo" visuallyHidden>
                  Título:
                </Form.Label>
                <Col sm={10}>
                  <InputGroup>
                    <InputGroup.Text>Título:</InputGroup.Text>
                    <Form.Control type="text" id="tituloFora" readOnly 
                    value={tituloVotavao} />
                  </InputGroup>
                </Col>
              </Form.Group>
              <Form.Group as={Row} className="mb-2" controlId="formHorizontalDescricao">
                <Form.Label htmlFor="inlineFormInputGroupDescricao" visuallyHidden>
                  Descrição:
                </Form.Label>
                <Col sm={10}>
                  <InputGroup>
                    <InputGroup.Text>Descrição:</InputGroup.Text>
                    <Form.Control as="textarea" rows={2} type="text" id="descricaoFora" readOnly
                    value={descricaoVotavao} />
                  </InputGroup>
                </Col>
              </Form.Group>
            </Form>
            <Row xs="auto" hidden={eDev()}>
              <Col>
                 <Button variant="primary" 
                         onClick={() => {handleBuscaVotacao(); iniciaVotacao();}}
                         disabled={iniciaVotos}
                         >
                           Iniciar votação
                 </Button>
              </Col>
              <Col>
                 <Button variant="primary" 
                          onClick={() => {
                            handleBuscaVotacao();
                            finalizaVotacao();

                          }}
                          disabled={finalizaVotos}>
                          
                    Finalizar votação
                  </Button>
              </Col>
            </Row>
          </Col>
          <Col style={{ textAlign: "right", padding: "20px 20px" }}>
            <OffCanvasChat/>
            <CadastroAtividade hidden={eDev()} 
              handleAddAtividade={handleAddAtividade}
              atividades={atividades}
              handleVoteClick={handleVoteClick}
              handleExcluirAtividade={handleExcluirAtividade}
              handleVoteClickAction={handleVoteClickAction}
              nenhumaAtividade={nenhumaAtividade}
            />
          </Col>
        </Row>
      </Container>
      <Container hidden={cardsVotos}>
        <Row xs="auto">
          {votacao.map((item) => {
            return [
              <Col>
                <Card style={{ width: '5rem' }}>
                  <Card.Body className="text-center">
                    <Card.Title>{item.pontuacaoVotavao}</Card.Title>
                    <Card.Text>{item.nomeUsuario}</Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            ]
          })}
        </Row>
      </Container>
      <CardsVotacao cards={cardsStatus} setCards={setCardsStatus} votacao={setVotacao} atividadeId={atividadeId} />
    </>
  );
};

export default PlanningPoker;