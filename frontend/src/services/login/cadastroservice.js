import axios from 'axios';

class CadastroService {

    cadastro(usuario){
        return axios.post(`http://localhost:8081/usuario`, usuario);
    }
}
export default new CadastroService()