import axios from 'axios';
import { Component } from 'react';

export class AtividadeService extends Component{ 

    constructor(props) {
        super(props);
      }
    
    async cadastroAtividade(atividade){
        //debugger;
        return await axios.put(`http://localhost:8081/planning`, atividade);
        
    }

    exclusaoAtividade(idAtividade){
        debugger;
        return axios.delete(`http://localhost:8081/planning`, idAtividade);
    }

    buscaUltimaAtividade(){
        //debugger;
        return axios.get(`http://localhost:8081/planning`);
    }
    
    deleteUltimaAtividade(){
        //debugger;
        return axios.delete(`http://localhost:8081/planning`);
    }

    updateVotacaoAtiva(){
        return axios.post(`http://localhost:8081/planning`);  
    }

}

export default new AtividadeService()