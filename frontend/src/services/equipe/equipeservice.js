import axios from 'axios';

class EquipeService {

    cadastroEquipe(equipe){
        debugger;
        return axios.post(`http://localhost:8081/equipe`, equipe);
    }

    cadastroUsuarioEquipe(usuario_equipe){
        debugger;
        return axios.post(`http://localhost:8081/equipe/usuario-equipe`, usuario_equipe);
    }

    consultarUsuariosEquipe(id_equipe){
        debugger;
        return axios.post(`http://localhost:8081/equipe/usuarios-da-equipe/${id_equipe}`);
    }

    consultarEquipe(id_usuario){
        debugger;
        return axios.post(`http://localhost:8081/equipe/lista-equipes/${id_usuario}`);
    }

    removerUsuariosEquipe(id_equipe, id_usuario) {
        debugger;
        return axios.post(`http://localhost:8081/equipe/remover/${id_equipe}/${id_usuario}`);
    }

    removerTodosUsuarios(id_equipe) {
        debugger;
        return axios.post(`http://localhost:8081/equipe/remover/${id_equipe}`);
    }
}

export default new EquipeService()