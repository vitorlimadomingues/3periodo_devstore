import axios from 'axios';

class VotacaoService {

    cadastrarVotacao(votacao){
        debugger;
        return axios.post(`http://localhost:8081/votacao`, votacao);
    }

    buscarVotacao(){
        return axios.get('http://localhost:8081/votacao');
    }

    finalizarAtividade(data){
        return axios.post(`http://localhost:8081/atividade`, data);
    }

}

export default new VotacaoService()