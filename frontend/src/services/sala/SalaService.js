import axios from 'axios';

class SalaService {

    criarSala(data){
        return axios.post(`http://localhost:8081/sala`, data);
    }

    buscarEquipe(id){
        return axios.get(`http://localhost:8081/equipe/${id}`)
    }

    buscarCerimonia(id){
        return axios.get(`http://localhost:8081/cerimonia/${id}`)
    }

    buscarEquipe(){
        return axios.get(`http://localhost:8081/equipe`)
    }

    buscarUsuariosEquipe(id){
        return axios.get(`http://localhost:8081/usuario/by-equipe/${id}`)
    }

    enviarEmail(email){
        return axios.post(`http://localhost:8081/equipe/email`, email)
    }

    buscaUltimaSala(){
        return axios.get(`http://localhost:8081/sala`);
    }
}
export default new SalaService()