import axios from 'axios';

class RetrospectivaService {

    criarAtividade(data){
        return axios.post(`http://localhost:8081/retrospectiva`, data);
    }

    excluirAtividade(id){
        return axios.delete(`http://localhost:8081/retrospectiva/${id}`);
    }
    
    buscarAtividadesSala(idSala){
        return axios.get(`http://localhost:8081/retrospectiva/${idSala}`);
    }

}
export default new RetrospectivaService()