import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/esm/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/esm/Col";
import { Link } from "react-router-dom";
import "./index.css";
import Button from "react-bootstrap/Button";
import SalaService from "./services/sala/SalaService";
import Swal from 'sweetalert2';

export default function PaginaInicialScrum() {


  //Verificar se o usuário está logado ao acessar a página ou se o usuário é um Scrum Master
  if (sessionStorage.getItem('id_usuario') === null) {
    Swal.fire({
      icon: 'info',
      title: 'Você não está logado!'
    }).then(function () {
      window.location = "/";
    });
  }

  let tipoSalaId = null;
  //Busca equipe para criar uma sala de Planning
  function buscarEquipe() {
    SalaService.buscarEquipe().
      then((response) => {
        if (response.data.length == 0) {
          Swal.fire('Nenhuma equipe cadastrada', '', 'warning')
        }
        else {
          Swal.fire({
            title: 'Deseja criar uma nova sala?',
            showDenyButton: true,
            confirmButtonText: 'Criar',
            denyButtonText: 'Cancelar',
          }).then((result) => {
            if (result.isConfirmed) {
              criarSala(response.data[0].idEquipe)
            }
          })
        }

      })
      .catch(() => {
        Swal.fire('Erro ao buscar equipe', '', 'error')
      })
  }

  //Criando sala Planning
  function criarSala(equipe) {
    let sala = {
      id: null,
      equipeDTO: {
        idEquipe: equipe
      },
      cerimoniaDTO: {
        id: tipoSalaId
      }
    }

    SalaService.criarSala(sala)
      .then((response) => {
        localStorage.setItem("idSala", response.data.id);
        enviarEmails(response.data, equipe)
      })
      .catch(() => {
        Swal.fire("Não foi possível criar sala", "", "error")
      })
  }

  function enviarEmails(sala, equipe) {
    let urlRedirect = tipoSalaId == 1 ? "planning-poker" : "retrospectiva"
    SalaService.buscarUsuariosEquipe(equipe)
      .then((response) => {
        let tipo = tipoSalaId == 1 ? "Planning" : "Retrospectiva"
        let url = "http://localhost:3000/" + urlRedirect + "/" + sala.id

        let email = {
          email: null,
          assunto: "Convite para " + tipo,
          corpo: "Olá, você foi convidado para participar de uma reunião de " + 
          tipo + ". \n Link de acesso: " + url
        }

        response.data.map((e) => {
          email.email = e.email;
          sendEmail(email)
        })
      })
      .catch(() => {
        Swal.fire("Não foi possível buscar a equipe selecionada", "", "error")
      })
      .finally(() => {
        Swal.fire("E-mail de convite enviado para os participantes da equipe", "", "success")
          .then(() => {
            window.location.href = urlRedirect + "/" + sala.id
          })
      })
  }

  function sendEmail(email) {
    SalaService.enviarEmail(email)
      .catch(() => {
        Swal.fire("Erro ao enviar E-mails", "", "error")
      })
  }

  const retornaTelaPerfil = () => {
    tipoSalaId = 1
    let idTipoUsuario = sessionStorage.getItem("id_tipo_usuario");
    if (idTipoUsuario === "1")
      buscarEquipe()
    else if (idTipoUsuario === "2") {

      SalaService.buscaUltimaSala().
        then(response => {
          sessionStorage.setItem("id_sala", response.data.id);
        });

      return "/planning-poker";
    }
  }

  const retornaTelaPlanning = () => {
    tipoSalaId = 2
    let idTipoUsuario = sessionStorage.getItem("id_tipo_usuario");
    if (idTipoUsuario === "1")
      buscarEquipe()
  }

  const retornaDev = () => {
    if (sessionStorage.getItem("id_tipo_usuario") === '2') {
      return true;
    }
    else {
      return false;
    }
  }

  return (
    <Container>
      <div id="cabecalho"></div>
      <Row className="justify-content-md-center">
        <Col></Col>
        <Col md="auto">
          <Row>
            <Col xs={6} md={4} style={{ textAlign: "center" }} hidden={retornaDev()}>
              <Button variant="primary" className="bot">
                <Link to="/CadastroEquipe">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    enable-background="new 0 0 24 24"
                    height="150"
                    viewBox="0 0 24 24"
                    width="150"
                    fill="#fff"
                  >
                    <rect fill="none" height="150" width="150" />
                    <g>
                      <path d="M12,12.75c1.63,0,3.07,0.39,4.24,0.9c1.08,0.48,1.76,1.56,1.76,2.73L18,18H6l0-1.61c0-1.18,0.68-2.26,1.76-2.73 C8.93,13.14,10.37,12.75,12,12.75z M4,13c1.1,0,2-0.9,2-2c0-1.1-0.9-2-2-2s-2,0.9-2,2C2,12.1,2.9,13,4,13z M5.13,14.1 C4.76,14.04,4.39,14,4,14c-0.99,0-1.93,0.21-2.78,0.58C0.48,14.9,0,15.62,0,16.43V18l4.5,0v-1.61C4.5,15.56,4.73,14.78,5.13,14.1z M20,13c1.1,0,2-0.9,2-2c0-1.1-0.9-2-2-2s-2,0.9-2,2C18,12.1,18.9,13,20,13z M24,16.43c0-0.81-0.48-1.53-1.22-1.85 C21.93,14.21,20.99,14,20,14c-0.39,0-0.76,0.04-1.13,0.1c0.4,0.68,0.63,1.46,0.63,2.29V18l4.5,0V16.43z M12,6c1.66,0,3,1.34,3,3 c0,1.66-1.34,3-3,3s-3-1.34-3-3C9,7.34,10.34,6,12,6z" />
                    </g>
                  </svg>
                </Link>
              </Button>
            </Col>
            <Col xs={6} md={4} style={{ textAlign: "center" }}>
              <Button variant="primary" className="bot" onClick={() => retornaTelaPerfil()}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  enable-background="new 0 0 24 24"
                  height="150"
                  viewBox="0 0 24 24"
                  width="150"
                  fill="#fff"
                >
                  <rect fill="none" height="150" width="150" />
                  <path d="M22,5.18L10.59,16.6l-4.24-4.24l1.41-1.41l2.83,2.83l10-10L22,5.18z M12,20c-4.41,0-8-3.59-8-8s3.59-8,8-8 c1.57,0,3.04,0.46,4.28,1.25l1.45-1.45C16.1,2.67,14.13,2,12,2C6.48,2,2,6.48,2,12s4.48,10,10,10c1.73,0,3.36-0.44,4.78-1.22 l-1.5-1.5C14.28,19.74,13.17,20,12,20z M19,15h-3v2h3v3h2v-3h3v-2h-3v-3h-2V15z" />
                </svg>
              </Button>
            </Col>
            <Col xs={6} md={4} style={{ textAlign: "center" }}>
                <Button variant="primary" className="bot" onClick={() => retornaTelaPlanning()}>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    height="150"
                    viewBox="0 0 24 24"
                    width="150"
                    fill="#fff"
                  >
                    <path d="M0 0h24v24H0z" fill="none" />
                    <path d="M3 13h8V3H3v10zm0 8h8v-6H3v6zm10 0h8V11h-8v10zm0-18v6h8V3h-8z" />
                  </svg>
                </Button>
            </Col>
          </Row>
          <Row style={{ marginTop: "10px" }}>
            <Col style={{ textAlign: "center" }} hidden={retornaDev()}>Equipe</Col>
            <Col style={{ textAlign: "center" }}>Planning Poker</Col>
            <Col style={{ textAlign: "center" }}>Retrospectiva</Col>
          </Row>
        </Col>
        <Col></Col>
      </Row>
    </Container>
  );
}
