import React from "react"
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import Login from "./login";
import { BrowserRouter, Switch, Route} from 'react-router-dom';
import PaginaInicialScrum from "./PaginaInicialScrum";
import Cadastro from "./cadastro";
import CadastroEquipe from "./CadastroEquipe";
import PlanningPoker from "./PlanningPoker";
import Retrospectiva from "./retrospectiva";
import Sala from "./Sala";

const App = () => {

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/cadastro" component={Cadastro} />
        <Route path="/PaginaInicialScrum" component={PaginaInicialScrum} />
        <Route path="/CadastroEquipe" component={CadastroEquipe} />
        <Route path="/planning-poker" component={PlanningPoker} />
        <Route path="/retrospectiva" component={Retrospectiva} />
        <Route path="/sala" component={Sala} />
      </Switch>
    </BrowserRouter>
  );

}

export default App;
