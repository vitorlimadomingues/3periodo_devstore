import React from "react";
import { CloseButton, Row, Col, Card } from "react-bootstrap";

const UsuariosEquipe = ({ usuarios, handleRemoverUsuario }) => {

  const renderUsuariosEquipe = (u) => {
    return (
      <Card className="card">
        <Card.Body>
          <Row>
            <Col style={{ paddingTop: "10px" }}>
              <Card.Title>{u.email}</Card.Title>
            </Col>
            <Col style={{ paddingTop: "10px", align: "Right" }} align="Right">
            <CloseButton
              onClick={() => { handleRemoverUsuario(u.id) }}
              aFria-label="Hide"
              className="button-delete"
              variant="outline-danger"
              size="sm"
            ></CloseButton>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    )
  }

  return (
    <div className="UsuariosEquipe">
      {usuarios.map(renderUsuariosEquipe)}
    </div>
  );
}

export default UsuariosEquipe;
