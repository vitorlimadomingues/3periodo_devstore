import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import { Row, Col, ToggleButton } from "react-bootstrap";
import Navbar from "react-bootstrap/Navbar";
import Swal from "sweetalert2";
import "./CardsVotacao.css";
import VotacaoService from "./services/votacao/VotacaoService";

export default function CardsVotacao(props) {
  localStorage.setItem("idVotacao", 0);

  const retornaIdVotacao = () => {
    if (localStorage.getItem("idVotacao") !== 0)
      return localStorage.getItem("idVotacao");
    else return null;
  };

  const [toogle, setToogle] = React.useState(true);
  const [cor, setCor] = React.useState("");
  React.useEffect(() => {
    setCor((state) => (toogle ? "#adceff" : ""));
  }, [toogle]);

  const fazerVoto = (valorVoto) => {
    let votacao = {
      id: retornaIdVotacao(),
      idAtividade: props.atividadeId,
      idUsuario: sessionStorage.getItem("id_usuario"),
      pontuacaoVotacao: valorVoto,
    };

    document.getElementById(`cardNumero_${valorVoto}`).style.backgroundColor = cor;
    setToogle((state) => !state);

    VotacaoService.cadastrarVotacao(votacao)
      .then((response) => {
        if (response.status === 200) {
          console.log(response.data);
          localStorage.setItem("idVotacao", response.data.id);
        } else {
          Swal.fire({
            icon: "error",
            title: "Votacao não cadastrada",
          });
        }
      })
      .catch((response) => {
        Swal.fire({
          icon: "error",
          title: "Erro ao cadastrar votacao",
        });
      });
  };

  return (
    <>
      <Navbar
        className="navCards"
        fixed="bottom"
        hidden={props.cards}
        style={{ paddingBottom: 0 }}
      >
        <Container flat className="linhaCardsNumero justify-content-md-center">
          <Row xs="auto">
            {[
              "0",
              "1",
              "2",
              "3",
              "5",
              "8",
              "13",
              "21",
              "34",
              "55",
              "89",
              "?",
            ].map((numeroDoCard) => (
              <Col style={{ padding: "3px"}}>
                <Button
                  className="cardNumero"
                  id={`cardNumero_${numeroDoCard}`}
                  variant="outline-primary"
                  style={{ padding: 0}}
                  value={numeroDoCard}
                  onClick={(e) => fazerVoto(e.target.value)}
                >
                  {numeroDoCard}
                </Button>
              </Col>
            ))}
          </Row>
        </Container>
        <Container style={{ width: "40%" }}></Container>
      </Navbar>
    </>
  );
}

/**
 *
 * ----- Para uso futuro -----
 *
 * <Container>
            <ButtonToolbar>
                <Row>
                    <Col>
                        <ButtonGroup size="lg" className="me-2">
                            <Button style={{ width: "100px", height: "100px" }}>1</Button>

                        </ButtonGroup>
                    </Col>
                    <Col>
                        <ButtonGroup size="lg" className="me-2">
                            <Button style={{ width: "100px", height: "100px" }}>2</Button>
                        </ButtonGroup>
                    </Col>
                    <Col>
                        <ButtonGroup size="lg" className="me-2">
                            <Button style={{ width: "100px", height: "100px" }}>3</Button>
                        </ButtonGroup>
                    </Col>
                </Row>
            </ButtonToolbar>
            <br />
            <ButtonToolbar>
                <Row>
                    <Col>
                        <ButtonGroup size="lg" className="me-2">
                            <Button style={{ width: "100px", height: "100px" }}>5</Button>

                        </ButtonGroup>
                    </Col>
                    <Col>
                        <ButtonGroup size="lg" className="me-2">
                            <Button style={{ width: "100px", height: "100px" }}>8</Button>
                        </ButtonGroup>
                    </Col>
                    <Col>
                        <ButtonGroup size="lg" className="me-2">
                            <Button style={{ width: "100px", height: "100px" }}>13</Button>
                        </ButtonGroup>
                    </Col>
                </Row>
            </ButtonToolbar>
            <br />
            <ButtonToolbar>
                <Row>
                    <Col style={{ width: "100px", height: "100px", paddingRight: "75px" }}>
                    </Col>
                    <Col>
                        <ButtonGroup size="lg" className="me-2">
                            <Button style={{ width: "100px", height: "100px" }}>Café</Button>
                        </ButtonGroup>
                    </Col>
                    <Col style={{ width: "100px", height: "100px" }}>

                    </Col>
                </Row>
            </ButtonToolbar>
        </Container>
 */
