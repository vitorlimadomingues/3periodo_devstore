-- Criando Banco de Dados
create database planning;
create user planning WITH ENCRYPTED password 'planning';
GRANT ALL PRIVILEGES ON database planning to planning;

-- Criando Tabelas
CREATE TABLE tipo_usuario(
    id_tipo_usuario SERIAL NOT NULL,
    descricao_tipo_usuario varchar(20),
    CONSTRAINT PK_ID_TIPO_USUARIO PRIMARY KEY(id_tipo_usuario)
);

CREATE TABLE usuario(
    id_usuario SERIAL NOT NULL,
    nome_usuario varchar(40),
    email_usuario varchar(40) NOT NULL,
    senha_usuario varchar(20),
    id_tipo_usuario integer,
    CONSTRAINT PK_ID_USUARIO PRIMARY KEY(id_usuario),
    CONSTRAINT FK_ID_TIPO_USUARIO FOREIGN KEY (id_tipo_usuario)
        REFERENCES tipo_usuario(id_tipo_usuario)
);

CREATE TABLE equipe(
    id_equipe SERIAL NOT NULL,
    id_scrum integer NOT NULL,
    nome_equipe varchar(20),
    data_criacao_equipe TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT FK_ID_USUARIO_EQUIPE FOREIGN KEY (id_scrum)
        REFERENCES usuario(id_usuario),
    CONSTRAINT PK_ID_EQUIPE_USUARIO PRIMARY KEY (id_equipe)

);

CREATE TABLE usuario_equipe(
    id_usuario_equipe SERIAL NOT NULL,
    id_equipe integer,
    id_usuario integer,
    CONSTRAINT FK_ID_EQUI_USU FOREIGN KEY (id_equipe)
        REFERENCES equipe(id_equipe),
    CONSTRAINT FK_ID_USU_EQUI FOREIGN KEY (id_usuario)
        REFERENCES usuario(id_usuario)
);

CREATE TABLE cerimonia(
    id_cerimonia SERIAL NOT NULL,
    descricacao_cerimonia varchar(20),
    CONSTRAINT PK_ID_CERIMONIA PRIMARY KEY (id_cerimonia)
);

CREATE TABLE sala(
    id_sala SERIAL NOT NULL,
    id_equipe INTEGER NOT NULL ,
    id_cerimonia INTEGER NOT NULL,
    data_criacao_sala TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT FK_ID_EQUIPE_SALA FOREIGN KEY (id_equipe)
        REFERENCES equipe(id_equipe),
    CONSTRAINT FK_ID_CERIMONIA_SALA FOREIGN KEY (id_cerimonia)
        REFERENCES cerimonia(id_cerimonia),
    CONSTRAINT FK_ID_SALA
        PRIMARY KEY(id_sala)
);

CREATE TABLE atividade(
    id_atividade SERIAL NOT NULL,
    id_sala integer NOT NULL,
    titulo_atividade varchar(20),
    descricao_atividade varchar(500),
    pontuacao_atividade integer,
    CONSTRAINT FK_ID_SALA_ATIVIDADE FOREIGN KEY (id_sala)
        REFERENCES sala(id_sala),
    CONSTRAINT PK_ID_atividade_sala PRIMARY KEY (id_atividade)
);

CREATE TABLE votacao(
    id_votacao SERIAL NOT NULL,
    id_atividade INTEGER NOT NULL,
    id_usuario INTEGER NOT NULL,
    pontuacao_votacao INTEGER NOT NULL,
    CONSTRAINT FK_ID_ATIVADE_VOTACAO FOREIGN KEY (id_atividade)
        REFERENCES atividade(id_atividade),
    CONSTRAINT FK_ID_USUARIO_VOTACAO FOREIGN KEY (id_usuario)
        REFERENCES usuario(id_usuario),
    CONSTRAINT FK_ID_VOTACAO_ATIVIDADE_USUARIO
        PRIMARY KEY(id_votacao)
);

CREATE TABLE status_usuario(
    id_status_usuario SERIAL NOT NULL,
    status varchar(20),
    descricao_status_usuario varchar(100),
        CONSTRAINT PK_ID_STATUS PRIMARY KEY(id_status_usuario)
);

insert into cerimonia (descricacao_cerimonia) values ('Planning');
insert into cerimonia (descricacao_cerimonia) values ('Retrospectiva');
insert into tipo_usuario (descricao_tipo_usuario) values ('Scrum Master');
insert into tipo_usuario (descricao_tipo_usuario) values ('Dev');

insert into status_usuario (status, descricao_status_usuario) values ('ativo', 'Usuário cadastrado e em uso');
insert into status_usuario (status, descricao_status_usuario) values ('pre_cadastro', 'Usuário do tipo "dev", pré-cadastrado pelo Scrum Master');
insert into status_usuario (status, descricao_status_usuario) values ('removido', 'Usuário do tipo "dev", que foi removido de uma equipe pelo SM');

--insert into usuario(nome, email, senha, tipo) values('Administrador', 'administrador@devstore.com.br', 'adm123', 'adm');

alter table cerimonia
rename column descricacao_cerimonia to descricao_cerimonia;

alter table usuario
    add id_status_usuario integer;

alter table usuario
    add constraint fk_id_status_usuario
        foreign key (id_status_usuario) references status_usuario;

-- adicionando parte da retrospectiva

CREATE TABLE atividade_retro(
    id_atividade_retro SERIAL NOT NULL,
    id_sala integer,
    titulo_atividade varchar(250) NOT NULL,
    coluna_atividade varchar(20) NOT NULL,
    CONSTRAINT FK_ID_SALA_ATIVIDADE_RETRO FOREIGN KEY (id_sala)
        REFERENCES sala(id_sala),
    CONSTRAINT PK_ID_atividade_retro PRIMARY KEY (id_atividade_retro)
);