package br.com.fatec.devstore.Planning.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SalaDTO {

    private Long id;

    private EquipeDTO equipeDTO;

    private CerimoniaDTO cerimoniaDTO;

    private LocalDateTime dataCriacaoSala;

}
