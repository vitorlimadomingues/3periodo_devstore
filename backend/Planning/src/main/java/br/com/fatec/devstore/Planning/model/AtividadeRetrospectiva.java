package br.com.fatec.devstore.Planning.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Builder
@Table(name = "atividade_retro")
public class AtividadeRetrospectiva {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_atividade_retro")
    private Long idAtividade;

    @Column(name = "id_sala")
    private Long idSala;

    @Column(name = "titulo_atividade", nullable = false)
    private String tituloAtividade;

    @Column(name = "coluna_atividade", nullable = false)
    private String colunaAtividade;

}
