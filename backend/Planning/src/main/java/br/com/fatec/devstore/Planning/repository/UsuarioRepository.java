package br.com.fatec.devstore.Planning.repository;

import br.com.fatec.devstore.Planning.model.Usuario;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Usuario findByEmail(String email);
    
    Optional<Usuario> findById(Long id);

    @Query(value = "select e.* from usuario e left join usuario_equipe ue on ue.id_usuario = e.id_usuario where ue.id_equipe = :idEquipe and e.id_status_usuario != 3", nativeQuery = true)
    List<Usuario> findAllUsuarios(@Param("idEquipe") Long idEquipe);

    @Transactional
    @Modifying
    @Query(value = "update usuario u set id_status_usuario = 3 from usuario_equipe ue where ue.id_usuario = u.id_usuario and ue.id_equipe = :idEquipe and u.id_usuario = :idUsuario", nativeQuery = true)
    int removerUsuario(@Param("idEquipe") Long idEquipe, @Param("idUsuario") Long idUsuario);

    @Transactional
    @Modifying
    @Query(value = "update usuario u set id_status_usuario = 3 from usuario_equipe ue where ue.id_usuario = u.id_usuario and ue.id_equipe = :idEquipe", nativeQuery = true)
    int removerTodosUsuarios(@Param("idEquipe") Long idEquipe);
}

