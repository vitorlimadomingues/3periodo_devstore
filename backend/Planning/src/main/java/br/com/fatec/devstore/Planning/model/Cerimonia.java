package br.com.fatec.devstore.Planning.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "cerimonia", schema = "public")
public class Cerimonia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cerimonia")
    private Long id;

    @Column(name = "descricao_cerimonia")
    private String descricao;

}
