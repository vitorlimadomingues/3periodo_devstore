package br.com.fatec.devstore.Planning.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "votacao")
public class Votacao {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_votacao")
    private Long id;
	
	@Column(name = "id_atividade")
    private Long idAtividade;
	
	@Column(name = "id_usuario")
    private Long idUsuario;
	
	@Column(name = "pontuacao_votacao")
	private Integer pontuacaoVotacao;

}
