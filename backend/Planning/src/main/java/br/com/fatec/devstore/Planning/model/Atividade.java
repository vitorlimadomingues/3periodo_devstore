package br.com.fatec.devstore.Planning.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Builder
@Table(name = "atividade")
public class Atividade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_atividade")
    private Long idAtividade;

    @Column(name = "id_sala", nullable = false)
    private Long idSala;

    @Column(name = "titulo_atividade", nullable = false)
    private String tituloAtividade;

    @Column(name = "descricao_atividade", nullable = false)
    private String descricaoAtividade;

    @Column(name = "pontuacao_atividade", nullable = false)
    private int pontuacaoAtividade;
    
    @Column(name = "votacao_ativa", nullable = false)
    private Boolean votacaoAtiva;
}
