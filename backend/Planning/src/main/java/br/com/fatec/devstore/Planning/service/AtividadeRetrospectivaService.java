package br.com.fatec.devstore.Planning.service;

import br.com.fatec.devstore.Planning.dto.AtividadeDTO;
import br.com.fatec.devstore.Planning.dto.AtividadeRetrospectivaDTO;
import br.com.fatec.devstore.Planning.model.Atividade;
import br.com.fatec.devstore.Planning.model.AtividadeRetrospectiva;
import br.com.fatec.devstore.Planning.repository.AtividadeRepository;
import br.com.fatec.devstore.Planning.repository.AtividadeRetrospectivaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AtividadeRetrospectivaService {

    @Autowired
    AtividadeRetrospectivaRepository atividadeRetrospectivaRepository;

    public AtividadeRetrospectiva save(AtividadeRetrospectiva atividadeRetrospectiva){
        return atividadeRetrospectivaRepository.save(atividadeRetrospectiva);
    }

    public void delete(Long id){
        AtividadeRetrospectiva atividadeRetrospectiva = atividadeRetrospectivaRepository.getById(id);
        atividadeRetrospectivaRepository.delete(atividadeRetrospectiva);
    }

    public AtividadeRetrospectiva toEntity(AtividadeRetrospectivaDTO atividadeRetrospectivaDTO){
        return AtividadeRetrospectiva
                .builder()
                .idAtividade(atividadeRetrospectivaDTO.getId())
                .tituloAtividade(atividadeRetrospectivaDTO.getTituloAtividade())
                .idSala(atividadeRetrospectivaDTO.getIdSala())
                .colunaAtividade(atividadeRetrospectivaDTO.getColunaAtividade())
                .build();
    }

    public AtividadeRetrospectivaDTO toDTO(AtividadeRetrospectiva atividadeRetrospectiva){
        return AtividadeRetrospectivaDTO
                .builder()
                .id(atividadeRetrospectiva.getIdAtividade())
                .tituloAtividade(atividadeRetrospectiva.getTituloAtividade())
                .idSala(atividadeRetrospectiva.getIdSala())
                .colunaAtividade(atividadeRetrospectiva.getColunaAtividade())
                .build();
    }

    public List<AtividadeRetrospectivaDTO> toDTOList(List<AtividadeRetrospectiva> atividadeRetrospectivaList){
        return atividadeRetrospectivaList.stream().map(this::toDTO).collect(Collectors.toList());
    }
    
}
