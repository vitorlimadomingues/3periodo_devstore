package br.com.fatec.devstore.Planning.service;

import br.com.fatec.devstore.Planning.dto.UsuarioDTO;
import br.com.fatec.devstore.Planning.model.Usuario;
import br.com.fatec.devstore.Planning.model.UsuarioEquipe;
import br.com.fatec.devstore.Planning.repository.UsuarioEquipeRepository;
import br.com.fatec.devstore.Planning.repository.UsuarioRepository;
import br.com.fatec.devstore.Planning.tools.ValidarEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    UsuarioEquipeRepository usuarioEquipeRepository;

    public Usuario login(String email, String senha){
        Usuario usuario = usuarioRepository.findByEmail(email);

        if((usuario.getSenha().trim()).equals(senha)) {
        	return usuario;
        }
        
        return new Usuario();
        
    }

    public Usuario cadastrar(UsuarioDTO usuarioDTO){

        //Validação de E-mail
        ValidarEmail ve = new ValidarEmail();
        boolean validacaoEmail = ve.isValidEmailAddress(usuarioDTO.getEmail());
        boolean validacaoRegexEmail = ve.isValidEmailAddressRegex(usuarioDTO.getEmail());

        if(!validacaoEmail || !validacaoRegexEmail){
            System.out.println("E-mail inválido!");
            return null;
        }

        //Validação de senha
        if((usuarioDTO.getSenha() == null || usuarioDTO.getSenha().length() < 6) && usuarioDTO.getIdTipoUsuario() == 1){
            System.out.println("Senha inválida!");
            return null;
        }

        Usuario usuario = new Usuario(
                usuarioDTO.getId(),
                usuarioDTO.getNome(),
                usuarioDTO.getEmail(),
                usuarioDTO.getSenha(),
                usuarioDTO.getIdTipoUsuario(),
                usuarioDTO.getIdStatusUsuario()
        );

        return usuarioRepository.save(usuario);
    }

    public Usuario buscarUsuario(Long id){
        Optional<Usuario> optional = usuarioRepository.findById(id);
        return optional.orElseGet(() -> optional.orElse(Usuario.builder().build()));
    }

    public List<Usuario> buscarUsuariosEquipe(Long id) {
        List<Usuario> usuarios = usuarioRepository.findAllUsuarios(id);
        return usuarios;
    }

    public boolean removerUsuarioEquipe(Long idEquipe, Long idUsuario) {
        int result = usuarioRepository.removerUsuario(idEquipe, idUsuario);

        return result >= 1;
    }

    public boolean removerTodosUsuarios(Long idEquipe) {
        int result = usuarioRepository.removerTodosUsuarios(idEquipe);

        System.out.println(result);
        return result >= 1;
    }

    public List<UsuarioDTO> buscarUsuariosPorEquipe(Long id){
        List<UsuarioEquipe> usuarioEquipeList = usuarioEquipeRepository.findAllByIdEquipe(id);
        if(usuarioEquipeList.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Equipe não encontrada");
        }
        List<UsuarioDTO> usuarioDTOList = new ArrayList<>();
        for(UsuarioEquipe usuarioEquipe: usuarioEquipeList){
            Usuario usuario = buscarUsuario(usuarioEquipe.getIdUsuario());
            usuarioDTOList.add(toDTO(usuario));
        }
        return usuarioDTOList;
    }

    public UsuarioDTO toDTO(Usuario usuario){
        return UsuarioDTO
                .builder()
                .id(usuario.getId())
                .email(usuario.getEmail())
                .senha(usuario.getSenha())
                .nome(usuario.getNome())
                .idTipoUsuario(usuario.getIdTipoUsuario())
                .build();
    }
}
