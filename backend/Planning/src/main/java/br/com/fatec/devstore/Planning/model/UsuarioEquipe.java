package br.com.fatec.devstore.Planning.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "usuario_equipe")
public class UsuarioEquipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario_equipe", nullable = false)
    private Long id;

    @Column(name = "id_equipe", nullable = false)
    private Long idEquipe;

    @Column(name = "id_usuario", nullable = false)
    private Long idUsuario;
}
