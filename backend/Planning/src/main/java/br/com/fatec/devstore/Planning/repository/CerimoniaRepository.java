package br.com.fatec.devstore.Planning.repository;

import br.com.fatec.devstore.Planning.model.Cerimonia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CerimoniaRepository extends JpaRepository<Cerimonia, Long> {
}
