package br.com.fatec.devstore.Planning.model;

import java.util.Date;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UsuarioVotacao {
	
	@JsonProperty
	private Long id;
	
	@JsonProperty
	private String nomeUsuario;
	
	@JsonProperty
	private String emailUsuario;
	
	@JsonProperty
	private Integer pontuacaoVotavao;
}
