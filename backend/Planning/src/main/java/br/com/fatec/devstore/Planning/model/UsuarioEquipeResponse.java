package br.com.fatec.devstore.Planning.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UsuarioEquipeResponse {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String name;
    @JsonProperty
    private Long idEquipe;

    public String getName(){
         return name;
     }

    public Long getIdEquipe(){
        return idEquipe;
    }
}
