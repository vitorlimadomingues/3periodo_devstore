package br.com.fatec.devstore.Planning.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "equipe")
public class Equipe {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_equipe")
	private Long idEquipe;
	
	@Column(name = "id_scrum", nullable = false)
	private Long idScrum;
	
	@Column(name = "nome_equipe", nullable = false)
	private String nomeEquipe;
	
	@Column(name = "data_criacao_equipe", nullable = false)
	private Date dataCriacaoEquipe;	
	
}
