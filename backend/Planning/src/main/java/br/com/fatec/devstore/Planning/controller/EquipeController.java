package br.com.fatec.devstore.Planning.controller;

import br.com.fatec.devstore.Planning.dto.EmailDTO;
import br.com.fatec.devstore.Planning.dto.UsuarioDTO;
import br.com.fatec.devstore.Planning.dto.UsuarioEquipeDTO;
import br.com.fatec.devstore.Planning.email.SpringEmailMain;
import br.com.fatec.devstore.Planning.model.Equipe;
import br.com.fatec.devstore.Planning.model.Usuario;
import br.com.fatec.devstore.Planning.model.UsuarioEquipeResponse;
import br.com.fatec.devstore.Planning.service.UsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import br.com.fatec.devstore.Planning.dto.EquipeDTO;
import br.com.fatec.devstore.Planning.service.EquipeService;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/equipe")
public class EquipeController {

    @Autowired
	EquipeService equipeService;
    @Autowired
    UsuarioService usuarioService;
	
	@RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Equipe> cadastrarEquipe(@RequestBody EquipeDTO equipeDTO){
        Equipe equipe = equipeService.cadastrarEquipe(equipeDTO);
        return new ResponseEntity<>(equipe, HttpStatus.OK);
    }

    @RequestMapping(value = "/usuario-equipe", method=RequestMethod.POST)
    public ResponseEntity<?> cadastrarUsuarioEquipe(@RequestBody List<UsuarioEquipeResponse> membros){

        for (int m = 0; m < membros.size(); m += 1) {
            UsuarioDTO usuarioDTO = new UsuarioDTO(null, null, membros.get(m).getName(), null, 2, 2);
            Usuario u = usuarioService.cadastrar(usuarioDTO);

            UsuarioEquipeDTO usuarioEquipeDTO = new UsuarioEquipeDTO(null, membros.get(m).getIdEquipe(), u.getId());
            equipeService.cadastrarUsuarioEquipe(usuarioEquipeDTO);
        }
        return new ResponseEntity<>( "Equipe cadastrada com sucesso!", HttpStatus.OK);
    }

    @PostMapping(value = "/email")
    public void send(@RequestBody EmailDTO emailDTO){
        System.out.println("E-mail enviado para " + emailDTO.getEmail());
        SpringEmailMain.main(emailDTO.getEmail(), emailDTO.getAssunto(), emailDTO.getCorpo());
    }

    //Buscar Equipe pelo ID da Equipe
    @GetMapping(value = "/busca-equipe/{id}")
    public EquipeDTO buscarEquipe(@PathVariable("id") Long id){
	    return equipeService.buscarEquipe(id);
    }

    //Consultar usuários
    @RequestMapping(value = "/usuarios-da-equipe/{id}", method=RequestMethod.POST)
    public ResponseEntity<List<Usuario>> buscarUsuariosEquipe (@PathVariable("id") Long idScrumMaster) {

        List<Usuario> usuarios = usuarioService.buscarUsuariosEquipe(idScrumMaster);
        return new ResponseEntity<>(usuarios, HttpStatus.OK);
    }

    //Consulta as equipes dos SM
    @RequestMapping(value = "/lista-equipes/{id}", method=RequestMethod.POST)
    public ResponseEntity<Equipe> buscarEquipesScrumMaster (@PathVariable("id") Long idScrumMaster) {

        Equipe equipe = equipeService.buscarEquipeDoSm(idScrumMaster);
        return new ResponseEntity<>(equipe, HttpStatus.OK);
    }

    //Remover um usuario da equipe
    @RequestMapping(value = "/remover/{id_equipe}/{id_usuario}", method=RequestMethod.POST)
    public ResponseEntity<?> removerUsuarioEquipe (@PathVariable("id_equipe") Long idEquipe, @PathVariable("id_usuario") Long idUsuario) {
        boolean result = usuarioService.removerUsuarioEquipe(idEquipe, idUsuario);

        if (result) {
            return new ResponseEntity<>("Usuário removido!", HttpStatus.OK);
        }
        return new ResponseEntity<>("Usuário não foi removido ou não existe!", HttpStatus.NOT_MODIFIED);
    }

    //Remover todos os usuários da equipe
    @RequestMapping(value = "/remover/{id_equipe}", method=RequestMethod.POST)
    public ResponseEntity<?> removerTodosUsuarios (@PathVariable("id_equipe") Long idEquipe) {
        boolean result = usuarioService.removerTodosUsuarios(idEquipe);

        if (result) {
            return new ResponseEntity<>("Usuários removidos!", HttpStatus.OK);
        }
        return new ResponseEntity<>("Usuários não foram removidos ou a equipe não tem usuários!", HttpStatus.NOT_MODIFIED);
    }

    @GetMapping
    public List<EquipeDTO> listarEquipes(){
	    return equipeService.listarEquipes();
    }

    @GetMapping("/{id}")
    public EquipeDTO buscarEquipeById(@PathVariable("id") Long id){
	    EquipeDTO equipeDTO = equipeService.buscarEquipe(id);
	    if(equipeDTO == null){
	        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Equipe não encontrada");
        }
	    return equipeDTO;
    }

}
