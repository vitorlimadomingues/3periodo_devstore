package br.com.fatec.devstore.Planning.service;

import br.com.fatec.devstore.Planning.dto.SalaDTO;
import br.com.fatec.devstore.Planning.model.Cerimonia;
import br.com.fatec.devstore.Planning.model.Equipe;
import br.com.fatec.devstore.Planning.model.Sala;
import br.com.fatec.devstore.Planning.repository.SalaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class SalaService {

    @Autowired
    SalaRepository salaRepository;

    @Autowired
    CerimoniaService cerimoniaService;

    @Autowired
    EquipeService equipeService;

    public SalaDTO criarSala(SalaDTO salaDTO){
        Equipe equipe = equipeService.buscarEquipeEntity(salaDTO.getEquipeDTO().getIdEquipe());
        Cerimonia cerimonia = cerimoniaService.buscarCerimoniaEntity(salaDTO.getCerimoniaDTO().getId());
        Sala sala = new Sala();
        sala.setEquipe(equipe);
        sala.setCerimonia(cerimonia);
        sala.setDataCriacaoSala(LocalDateTime.now());
        Sala saved = salaRepository.save(sala);
        return toDTO(saved);
    }

    public SalaDTO toDTO(Sala sala){
        return SalaDTO
                .builder()
                .id(sala.getId())
                .dataCriacaoSala(sala.getDataCriacaoSala())
                .cerimoniaDTO(cerimoniaService.toDTO(sala.getCerimonia()))
                .equipeDTO(equipeService.toDTO(sala.getEquipe()))
                .build();
    }
    
    public Sala retornaUltimaSala() {
    	return salaRepository.findTopByOrderByIdDesc();
    }

}
