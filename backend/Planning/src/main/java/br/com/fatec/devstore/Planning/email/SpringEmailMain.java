package br.com.fatec.devstore.Planning.email;

import br.com.fatec.devstore.Planning.PlanningApplication;
import br.com.fatec.devstore.Planning.email.envio.Mailer;
import br.com.fatec.devstore.Planning.email.envio.Mensagem;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class SpringEmailMain {

    public static void main(String email, String assunto, String corpo) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(
                SpringEmailMain.class.getPackage().getName());

        Mailer mailer = applicationContext.getBean(Mailer.class);
        mailer.enviar(new Mensagem("DevStore <devstorefatec@gmail.com>",
                Arrays.asList(String.format("Dev <%s>", email)), assunto, corpo));

        applicationContext.close();
    }
}
