package br.com.fatec.devstore.Planning.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class AtividadeRetrospectivaDTO {

    private Long id;

    private Long idSala;

    private String tituloAtividade;

    private String colunaAtividade;

}
