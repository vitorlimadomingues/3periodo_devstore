package br.com.fatec.devstore.Planning.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sala")
public class Sala {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_sala")
    private Long id;

    @Column(name = "data_criacao_sala")
    private LocalDateTime dataCriacaoSala;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_equipe")
    private Equipe equipe;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_cerimonia")
    private Cerimonia cerimonia;
}
