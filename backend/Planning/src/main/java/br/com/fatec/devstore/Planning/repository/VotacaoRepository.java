package br.com.fatec.devstore.Planning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fatec.devstore.Planning.model.Votacao;

public interface VotacaoRepository extends JpaRepository<Votacao, Long>{
	
	List<Votacao> findAll();
	
}
