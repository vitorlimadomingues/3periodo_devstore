package br.com.fatec.devstore.Planning.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fatec.devstore.Planning.model.Equipe;

public interface EquipeRepository extends JpaRepository<Equipe, Long>{

    Equipe findByIdScrum(Long id);
}
