package br.com.fatec.devstore.Planning.service;

import br.com.fatec.devstore.Planning.dto.UsuarioEquipeDTO;
import br.com.fatec.devstore.Planning.model.UsuarioEquipe;
import br.com.fatec.devstore.Planning.repository.UsuarioEquipeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.fatec.devstore.Planning.dto.EquipeDTO;
import br.com.fatec.devstore.Planning.model.Equipe;
import br.com.fatec.devstore.Planning.repository.EquipeRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EquipeService {
	
	@Autowired
	EquipeRepository equipeRepository;
	@Autowired
	UsuarioEquipeRepository usuarioEquipeRepository;
	
	public Equipe cadastrarEquipe(EquipeDTO equipeDTO) {
		Equipe equipe = new Equipe(
				equipeDTO.getIdEquipe(),
				equipeDTO.getIdScrum(),
				equipeDTO.getNomeEquipe(),
				new Date()
		);

		return equipeRepository.save(equipe);
	}

	public void cadastrarUsuarioEquipe(UsuarioEquipeDTO usuarioEquipeDTO){
		UsuarioEquipe usuarioEquipe = new UsuarioEquipe(
				usuarioEquipeDTO.getId(),
				usuarioEquipeDTO.getIdEquipe(),
				usuarioEquipeDTO.getIdUsuario()
		);

		usuarioEquipeRepository.save(usuarioEquipe);
	}

	public EquipeDTO buscarEquipe(Long id){
		return toDTO(buscarEquipeEntity(id));
	}

	public Equipe buscarEquipeEntity(Long id){
		Optional<Equipe> optional = equipeRepository.findById(id);
		return optional.orElseGet(() -> optional.orElse(Equipe.builder().build()));
	}

	public List<EquipeDTO> listarEquipes(){
		return toDTOList(equipeRepository.findAll());
	}

	public EquipeDTO toDTO(Equipe equipe){
		return EquipeDTO
				.builder()
				.idEquipe(equipe.getIdEquipe())
				.dataCriacaoEquipe(equipe.getDataCriacaoEquipe())
				.nomeEquipe(equipe.getNomeEquipe())
				.idScrum(equipe.getIdScrum())
				.build();
	}

	public Equipe buscarEquipeDoSm(Long id) {
		return equipeRepository.findByIdScrum(id);
	}

	public List<EquipeDTO> toDTOList(List<Equipe> equipeList){
		return equipeList.stream().map(this::toDTO).collect(Collectors.toList());
	}
}
