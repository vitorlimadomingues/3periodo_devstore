package br.com.fatec.devstore.Planning.service;

import br.com.fatec.devstore.Planning.dto.CerimoniaDTO;
import br.com.fatec.devstore.Planning.model.Cerimonia;
import br.com.fatec.devstore.Planning.model.Usuario;
import br.com.fatec.devstore.Planning.repository.CerimoniaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CerimoniaService {

    @Autowired
    CerimoniaRepository cerimoniaRepository;

    public CerimoniaDTO save(CerimoniaDTO cerimoniaDTO){
        Cerimonia cerimonia = new Cerimonia(null, cerimoniaDTO.getDescricao());
        Cerimonia cerimoniaSaved = cerimoniaRepository.save(cerimonia);
        return new CerimoniaDTO(cerimoniaDTO.getId(), cerimoniaSaved.getDescricao());
    }

    public Cerimonia buscarCerimoniaEntity(Long id){
        Optional<Cerimonia> optional = cerimoniaRepository.findById(id);
        return optional.orElseGet(() -> optional.orElse(Cerimonia.builder().build()));
    }

    public CerimoniaDTO buscarCerimonia(Long id){
        return toDTO(buscarCerimoniaEntity(id));
    }

    public CerimoniaDTO toDTO(Cerimonia cerimonia){
        return CerimoniaDTO
                .builder()
                .id(cerimonia.getId())
                .descricao(cerimonia.getDescricao())
                .build();
    }

    public Cerimonia toEntity(CerimoniaDTO cerimoniaDTO){
        return Cerimonia
                .builder()
                .id(cerimoniaDTO.getId())
                .descricao(cerimoniaDTO.getDescricao())
                .build();
    }
}
