package br.com.fatec.devstore.Planning.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fatec.devstore.Planning.dto.VotacaoDTO;
import br.com.fatec.devstore.Planning.model.Usuario;
import br.com.fatec.devstore.Planning.model.UsuarioVotacao;
import br.com.fatec.devstore.Planning.model.Votacao;
import br.com.fatec.devstore.Planning.repository.UsuarioRepository;
import br.com.fatec.devstore.Planning.repository.VotacaoRepository;

@Service
public class VotacaoService {
	
	@Autowired
	VotacaoRepository votacaoRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	public Votacao cadastrar(VotacaoDTO votacaoDTO){
        Votacao votacao = new Votacao(
        		votacaoDTO.getId(), 
        		votacaoDTO.getIdAtividade(), 
        		votacaoDTO.getIdUsuario(),
        		votacaoDTO.getPontuacaoVotacao()
        		);

        return votacaoRepository.saveAndFlush(votacao);
    }
	
    public List<UsuarioVotacao> buscarVotacao(){
    	List<UsuarioVotacao> usuarioVotacacoes = new ArrayList<UsuarioVotacao>();    	
    	
    	List<Votacao> votacoes = (List<Votacao>) votacaoRepository.findAll();
    	
    	for(Votacao v: votacoes) {
    		Optional<Usuario> u = usuarioRepository.findById(v.getIdUsuario());
    	
    		UsuarioVotacao usuarioVotacao = new UsuarioVotacao(
    				v.getIdUsuario(), 
    				u.get().getNome().trim(), 
    				u.get().getEmail().trim(), 
    				v.getPontuacaoVotacao());
    		usuarioVotacacoes.add(usuarioVotacao);
    	}
    	
    	return usuarioVotacacoes;
    }

}
