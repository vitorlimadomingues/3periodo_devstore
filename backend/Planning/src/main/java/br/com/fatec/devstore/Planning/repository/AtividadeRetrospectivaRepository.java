package br.com.fatec.devstore.Planning.repository;

import br.com.fatec.devstore.Planning.model.AtividadeRetrospectiva;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AtividadeRetrospectivaRepository
		extends JpaRepository<AtividadeRetrospectiva, Long> {

	List<AtividadeRetrospectiva> findByIdSala(Long id);
	
}
