package br.com.fatec.devstore.Planning.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CerimoniaDTO {

    private Long id;

    private String descricao;

}
