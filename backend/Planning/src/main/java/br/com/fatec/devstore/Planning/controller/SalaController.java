package br.com.fatec.devstore.Planning.controller;

import br.com.fatec.devstore.Planning.dto.SalaDTO;
import br.com.fatec.devstore.Planning.model.Sala;
import br.com.fatec.devstore.Planning.service.SalaService;
import org.springframework.beans.factory.annotation.Autowired;
import br.com.fatec.devstore.Planning.service.SalaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/sala")
public class SalaController {

    @Autowired
    SalaService salaService;

    @PostMapping
    public SalaDTO criarSala(@RequestBody @Valid SalaDTO salaDTO){
        return salaService.criarSala(salaDTO);
    }
    
    @RequestMapping(method=RequestMethod.GET)
    public Sala retornaUltimaSala() {
    	return salaService.retornaUltimaSala();
    }
}
