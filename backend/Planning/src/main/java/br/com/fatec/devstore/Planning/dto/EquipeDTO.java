package br.com.fatec.devstore.Planning.dto;

import java.util.Date;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EquipeDTO {
	
	private Long idEquipe;
	
	private Long idScrum;
	
	private String nomeEquipe;
	
	private Date dataCriacaoEquipe;	
	
}
