package br.com.fatec.devstore.Planning.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "usuario")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private Long id;

    @Column(name = "nome_usuario")
    private String nome;

    @Column(name = "email_usuario", nullable = false)
    private String email;

    @Column(name = "senha_usuario")
    private String senha;

    @Column(name = "id_tipo_usuario")
    private int idTipoUsuario;

    @Column(name = "id_status_usuario")
    private int idStatus;
}
