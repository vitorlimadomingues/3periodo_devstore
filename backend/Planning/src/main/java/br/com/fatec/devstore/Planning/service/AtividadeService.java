package br.com.fatec.devstore.Planning.service;

import br.com.fatec.devstore.Planning.dto.AtividadeDTO;
import br.com.fatec.devstore.Planning.model.Atividade;
import br.com.fatec.devstore.Planning.repository.AtividadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AtividadeService {

    @Autowired
    AtividadeRepository atividadeRepository;

    public AtividadeDTO save(AtividadeDTO atividadeDTO){
        Atividade atividade = toEntity(atividadeDTO);
        return toDTO(atividadeRepository.save(atividade));
    }

    public AtividadeDTO toDTO(Atividade atividade){
        return AtividadeDTO
                .builder()
                .id(atividade.getIdAtividade())
                .tituloAtividade(atividade.getTituloAtividade())
                .descricaoAtividade(atividade.getDescricaoAtividade())
                .pontuacaoAtividade(atividade.getPontuacaoAtividade())
                .idSala(atividade.getIdSala())
                .build();
    }

    public Atividade toEntity(AtividadeDTO atividadeDTO){
        return Atividade
                .builder()
                .idAtividade(atividadeDTO.getId())
                .tituloAtividade(atividadeDTO.getTituloAtividade())
                .descricaoAtividade(atividadeDTO.getDescricaoAtividade())
                .pontuacaoAtividade(atividadeDTO.getPontuacaoAtividade())
                .idSala(atividadeDTO.getIdSala())
                .build();
    }
    
}
