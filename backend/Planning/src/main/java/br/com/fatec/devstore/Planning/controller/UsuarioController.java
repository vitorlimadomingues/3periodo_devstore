package br.com.fatec.devstore.Planning.controller;

import br.com.fatec.devstore.Planning.dto.UsuarioDTO;
import br.com.fatec.devstore.Planning.model.Usuario;
import br.com.fatec.devstore.Planning.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @PostMapping(value = "/{email}/{senha}")
    public ResponseEntity<Usuario> login(@PathVariable("email") String email, @PathVariable("senha") String senha){
        Usuario usuario = usuarioService.login(email.trim(), senha.trim());
        return new ResponseEntity<>(usuario, HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Usuario> cadastrar(@RequestBody UsuarioDTO usuarioDTO){
        Usuario usuario = usuarioService.cadastrar(usuarioDTO);

        if(usuario != null){
            System.out.println("Usuário com ID " + usuario.getId() + " adicionado.");
            return new ResponseEntity<>(usuario, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @GetMapping("/by-equipe/{id}")
    public List<UsuarioDTO> buscarUsuariosPorEquipe(@PathVariable("id") Long id){
        return usuarioService.buscarUsuariosPorEquipe(id);
    }

}
