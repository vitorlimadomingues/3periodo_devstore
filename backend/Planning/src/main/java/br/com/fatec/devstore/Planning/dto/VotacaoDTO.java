package br.com.fatec.devstore.Planning.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VotacaoDTO {
	
	private Long id;

    private Long idAtividade;

    private Long idUsuario;

    private Integer pontuacaoVotacao;


}
