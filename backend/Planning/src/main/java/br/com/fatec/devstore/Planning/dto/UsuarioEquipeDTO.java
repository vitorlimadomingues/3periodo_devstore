package br.com.fatec.devstore.Planning.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UsuarioEquipeDTO {
    private Long id;

    private Long idEquipe;

    private Long idUsuario;
}
