package br.com.fatec.devstore.Planning.controller;

import br.com.fatec.devstore.Planning.dto.AtividadeRetrospectivaDTO;
import br.com.fatec.devstore.Planning.model.AtividadeRetrospectiva;
import br.com.fatec.devstore.Planning.repository.AtividadeRetrospectivaRepository;
import br.com.fatec.devstore.Planning.service.AtividadeRetrospectivaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/retrospectiva")
public class RetrospectivaController {

    @Autowired
    AtividadeRetrospectivaService atividadeRetrospectivaService;

    @Autowired
    AtividadeRetrospectivaRepository atividadeRetrospectivaRepository;

    @PostMapping
    public AtividadeRetrospectivaDTO criarAtividade(
            @RequestBody @Valid AtividadeRetrospectivaDTO atividadeRetrospectivaDTO){
        AtividadeRetrospectiva atividadeRetrospectiva = atividadeRetrospectivaService.toEntity(atividadeRetrospectivaDTO);
        AtividadeRetrospectiva saved = atividadeRetrospectivaService.save(atividadeRetrospectiva);
        return atividadeRetrospectivaService.toDTO(saved);
    }

    @DeleteMapping("/{id}")
    public void excluirAtividade(@PathVariable("id") Long id){
        atividadeRetrospectivaService.delete(id);
    }

    @GetMapping("/{idSala}")
    public List<AtividadeRetrospectivaDTO> listarPorSala(
            @PathVariable("idSala") Long idSala){
        List<AtividadeRetrospectiva> atividadeRetrospectivaList = atividadeRetrospectivaRepository.findByIdSala(idSala);
        return atividadeRetrospectivaService.toDTOList(atividadeRetrospectivaList);
    }

}
