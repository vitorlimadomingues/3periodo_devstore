package br.com.fatec.devstore.Planning.repository;

import br.com.fatec.devstore.Planning.model.Sala;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalaRepository extends JpaRepository<Sala, Long> {
	
	Sala findTopByOrderByIdDesc();
}
