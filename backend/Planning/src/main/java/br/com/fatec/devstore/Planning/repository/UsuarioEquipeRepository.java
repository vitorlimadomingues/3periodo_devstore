package br.com.fatec.devstore.Planning.repository;

import br.com.fatec.devstore.Planning.model.UsuarioEquipe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsuarioEquipeRepository extends JpaRepository<UsuarioEquipe, Long> {

    List<UsuarioEquipe> findAllByIdEquipe(Long id);
}
