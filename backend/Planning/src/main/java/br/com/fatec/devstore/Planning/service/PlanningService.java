package br.com.fatec.devstore.Planning.service;

import br.com.fatec.devstore.Planning.dto.AtividadeDTO;
import br.com.fatec.devstore.Planning.model.Atividade;
import br.com.fatec.devstore.Planning.repository.AtividadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanningService {

    @Autowired
    AtividadeRepository atividadeRepository;

    public Long cadastrarAtividade(AtividadeDTO atividadeDTO) {
        Atividade atividade = new Atividade(
                atividadeDTO.getId(),
                atividadeDTO.getIdSala(),
                atividadeDTO.getTituloAtividade(),
                atividadeDTO.getDescricaoAtividade(),
                atividadeDTO.getPontuacaoAtividade(),
                atividadeDTO.getVotacaoAtiva()
        );

        atividade = atividadeRepository.saveAndFlush(atividade);
        return atividade.getIdAtividade();
    }
    
    public void excluirUltimaAtividade() {
    	Atividade atividade = atividadeRepository.findTopByOrderByIdAtividadeDesc();
    	atividadeRepository.deleteById(atividade.getIdAtividade());
    	
    }
    
    public Atividade buscaUltimaAtividade() {
        return atividadeRepository.findTopByOrderByIdAtividadeDesc();
    }
    
    public void alterarVotacao() {
    	Atividade atividade = atividadeRepository.findTopByOrderByIdAtividadeDesc();
    	atividade.setVotacaoAtiva(!atividade.getVotacaoAtiva());
    	atividadeRepository.save(atividade);
        System.out.println(atividade.getPontuacaoAtividade());
    	System.out.println(atividade.getVotacaoAtiva());
    }
    
    
}
