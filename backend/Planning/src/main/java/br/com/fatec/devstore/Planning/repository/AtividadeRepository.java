package br.com.fatec.devstore.Planning.repository;

import br.com.fatec.devstore.Planning.model.Atividade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AtividadeRepository extends JpaRepository<Atividade, Long> {
	
	void deleteById(Long idAtividade);
	
	Atividade findTopByOrderByIdAtividadeDesc();
	
	
	
}
