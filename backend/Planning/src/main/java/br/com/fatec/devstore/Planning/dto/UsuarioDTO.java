package br.com.fatec.devstore.Planning.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UsuarioDTO {

    private Long id;

    private String nome;

    private String email;

    private String senha;

    private int idTipoUsuario;

    private int idStatusUsuario;

}
