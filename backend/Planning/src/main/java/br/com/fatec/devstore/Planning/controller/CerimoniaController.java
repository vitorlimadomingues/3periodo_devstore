package br.com.fatec.devstore.Planning.controller;

import br.com.fatec.devstore.Planning.dto.CerimoniaDTO;
import br.com.fatec.devstore.Planning.service.CerimoniaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/cerimonia")
public class CerimoniaController {

    @Autowired
    CerimoniaService cerimoniaService;

    @PostMapping
    public CerimoniaDTO salvarCerimonia(@RequestBody CerimoniaDTO cerimoniaDTO){
        return cerimoniaService.save(cerimoniaDTO);
    }

    @GetMapping("/{id}")
    public CerimoniaDTO buscarCerimonia(@PathVariable("id") Long id){
        return cerimoniaService.buscarCerimonia(id);
    }
}
