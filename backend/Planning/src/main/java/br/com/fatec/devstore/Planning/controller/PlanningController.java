package br.com.fatec.devstore.Planning.controller;

import br.com.fatec.devstore.Planning.dto.AtividadeDTO;
import br.com.fatec.devstore.Planning.model.Atividade;
import br.com.fatec.devstore.Planning.service.PlanningService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/planning")
public class PlanningController {

    @Autowired
    PlanningService planningService;

    @RequestMapping(method= RequestMethod.PUT)
    public ResponseEntity<?> cadastrar(@RequestBody AtividadeDTO atividadeDTO){
        Long idAtividade = planningService.cadastrarAtividade(atividadeDTO);
        return new ResponseEntity<>(idAtividade, HttpStatus.OK);
    }
    
    @RequestMapping(method= RequestMethod.DELETE)
    public ResponseEntity<?> deletarLastId(){
        planningService.excluirUltimaAtividade();
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
    
    @RequestMapping(method= RequestMethod.GET)
    public ResponseEntity<?> buscaUltimaAtividade(){
        Atividade atividade = planningService.buscaUltimaAtividade();
        return new ResponseEntity<>(atividade, HttpStatus.OK);
    }
    
    @RequestMapping(method= RequestMethod.POST)
    public ResponseEntity<?> alterarVotacaoAtiva(){
        planningService.alterarVotacao();
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    
}
