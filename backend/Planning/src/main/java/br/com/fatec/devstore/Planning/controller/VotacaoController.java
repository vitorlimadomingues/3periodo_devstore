package br.com.fatec.devstore.Planning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.fatec.devstore.Planning.dto.VotacaoDTO;
import br.com.fatec.devstore.Planning.model.UsuarioVotacao;
import br.com.fatec.devstore.Planning.model.Votacao;
import br.com.fatec.devstore.Planning.service.VotacaoService;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/votacao")
public class VotacaoController {
	
	@Autowired
	VotacaoService votacaoService;

	@RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<?> cadastrar(@RequestBody VotacaoDTO votacaoDTO){
        Votacao votacao = votacaoService.cadastrar(votacaoDTO);

        System.out.println("Votação criada " + votacao.getIdUsuario() + " adicionado.");
        return new ResponseEntity<>(votacao, HttpStatus.OK);
    }
	
	@RequestMapping(method=RequestMethod.GET)
    public ResponseEntity<?> buscar(){
        List<UsuarioVotacao> votacoes = votacaoService.buscarVotacao();

        return new ResponseEntity<>(votacoes, HttpStatus.OK);
    }
	
	
	
}
