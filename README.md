# API - 3º SEMESTRE
**Projeto Integrador do 3º Semestre de Banco de Dados FATEC 2021**
## Projeto _Planning_
<p align="center">
<img src="/Imagens PI/logo.jpg"/>

_________________________________________
## :trophy: Equipe
<img src="/Imagens PI/Equipe.png"/>


_________________________________________
## :office: Cliente
A **IACIT** é uma empresa brasileira, fundada em 1986, e com sede em São José dos Campos (SP), um importante polo da indústria aeroespacial do Brasil.
Possui capacitação tecnológica para o desenvolvimento de produtos e sistemas aplicados ao Auxílio do Controle e do Tráfego Aéreo e Marítimo; Defesa e Segurança Pública; Fábrica de Software; Meteorologia; Pesquisa, Desenvolvimento e Inovação e Telemetria.
Certificada como Empresa Estratégica de Defesa (EED), a IACIT deposita seu conhecimento técnico e o desenvolvimento tecnológico em produtos e sistemas de alta tecnologia, no mais puro estado da arte.

_________________________________________
## :computer: Sobre o Projeto
**Problema**
<br/>
Devido ao grande crescimento do _Home Office_, muitas empresas de tecnologia precisaram se adequar conforme as necessidades das equipes de desenvolvimento, envolvendo a parte de planejamento. No entanto, é notável a dificuldade em encontrar sistemas para auxiliar nesta questão, principalmente para times que trabalham utilizando a metodologia _Scrum_.
<br/>
**Solução**
<br/>
Desenvolvido pela DevStore, este projeto possui como objetivo contruir uma aplicação Web que contemple a apresentação de um sistema de _Planning_, com uma criação simples e direta de atividades e de um sistema de votação, além da parte de retrospectiva, onde o usuário poderá se cadastrar como Scrum Master ou entrar como desenvolvedor para a realização das votações.

_________________________________________
## :dart: Objetivos
- Entregar um sistema de gerenciamento e votação de atividades (_Planning poker_) funcional;
- Implementar o sistema de forma a garantir a experiência do usuário de forma interativa;
- Criar a visualização adequada para cada atividade com as respectivas cerimónias;
- Desenvolver um ambiente adequado a realização da retrospectiva do sistema;
- Permitir o cadastro de equipes na aplicação;
- Criar e manter uma base de dados com as informações conforme necessidade do cliente.

_________________________________________
## :wrench: Ferramentas e Tecnologias
<img src="/Imagens PI/Tecnologias.png">

_________________________________________
## :triangular_flag_on_post:  Product Backlog
Clique nas imagens para mais detalhes
<table>
   <tr>
    <td align="center">Sprint 1</td>
    <td align="center">Sprint 2</td>
    <td align="center">Sprint 3</td>
    <td align="center">Sprint 4</td>
  </tr>
     <tr>
    <td align="center"><img style="border-radius: 50%;" src="/Imagens PI/PBSprint1.jpg" width="300px;" alt=""/></td>
    <td align="center"><img style="border-radius: 50%;" src="/Imagens PI/PBSprint2.jpg" width="300px;" alt=""/></td>
    <td align="center"><img style="border-radius: 50%;" src="/Imagens PI/PBSprint3.PNG" width="300px;" alt=""/></td> 
    <td align="center"><img style="border-radius: 50%;" src="/Imagens PI/PBSprint4.PNG" width="300px;" alt=""/></td> 
  </tr>
</table>

_________________________________________
## :link: Documentação


**Sprint 1**
- <a href="https://gitlab.com/orl22/api_3-sem_devstore">Detalhes da Sprint</a>
- <a href="https://gitlab.com/vitorlimadomingues/3periodo_devstore/-/blob/main/Apresenta%C3%A7%C3%A3o/Video_Produto_Sprint_1.mp4">Vídeo de Apresentação</a>

**Sprint 2**
- <a href="https://gitlab.com/orl22/api_3-sem_devstore2">Detalhes da Sprint</a>
- <a href="https://gitlab.com/vitorlimadomingues/3periodo_devstore/-/blob/main/Apresenta%C3%A7%C3%A3o/Video_Produto_Sprint_2.mp4" >Vídeo de Apresentação</a>    

**Sprint 3**
- <a href="https://gitlab.com/orl22/api_3-sem_devstore3">Detalhes da Sprint</a>
- <a href="https://gitlab.com/vitorlimadomingues/3periodo_devstore/-/blob/main/Apresenta%C3%A7%C3%A3o/Video_Produto_Sprint_3.mp4">Video de Apresentação</a>



**Sprint 4**
- <a href="https://gitlab.com/orl22/api_3-sem_devstore4">Detalhes da Sprint</a>
- <a href="https://gitlab.com/vitorlimadomingues/3periodo_devstore/-/raw/main/Apresenta%C3%A7%C3%A3o/Video_Produto_Sprint_4.mkv">Video de Apresentação</a>



**Documentação de FGTI**

- <a href="https://gitlab.com/vitorlimadomingues/3periodo_devstore/-/blob/main/Documentos/BSC.JPG">BSC da equipe</a>
- <a href="https://gitlab.com/vitorlimadomingues/3periodo_devstore/-/blob/main/Documentos/Portifolio_de_servi%C3%A7os-FGTI.xlsx">Portifólio de Serviços</a>
- <a href="https://gitlab.com/vitorlimadomingues/3periodo_devstore/-/blob/main/Documentos/CatalogodeServicos-_FGTI.ods">Catalogo de Serviços</a>


## :bookmark_tabs: Controle das Atividades


- <a href="https://gitlab.com/vitorlimadomingues/3periodo_devstore/-/milestones">Lista de atividades realizadas por Sprints</a>



## :construction: Project Builder

**Passos para executar o projeto em automático**

<ul>
<li>npm install</li>
<li>npm start</li>
</ul>

**Passos para executar o chat em automático**

rodar no front
  <ul>
  <li>npm install socket.io-client uuid</li>
  </ul>

rodar na pasta server
 <ul> 
 <li>npm i socket.io koa</li>
 <li>npm init-y</li>
 <li>server-> package.json -> "start": "nodemon server"</li>
 <li> npm i-D nodemon</li>
 <li> Node server</li>
  </ul>
